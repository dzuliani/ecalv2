#ifndef ROOT_myParticle_cxx
#define ROOT_myParticle_cxx

#include <myParticle.h>

myParticle::myParticle() { 
	myProtoParticle(); 
	m_clusters.clear();
	m_matchedPart   = 0;
	m_isSig         = -1;
	m_pi0type       = 0;
}
myParticle::myParticle(TLorentzVector &mom, Double_t mass) { 
	myProtoParticle (mom, mass); 
	m_clusters.clear();
	m_matchedPart   = 0;
	m_isSig         = -1;
	m_pi0type       = 0;
}

void myParticle::getMomIfOrigV(Double_t &px, Double_t &py, Double_t &pz, Double_t x0, Double_t y0, Double_t z0){
	vector<array<Double_t,4>> Exyz;
	//cout << "*******************************************\n";
	//cout << "Vertex:" << x0 << "    " << y0 << "     " << z0 << endl;
	auto getExyz = [&Exyz](Cluster* cluster){
		Exyz.push_back({cluster->getCalibE(), cluster->getCalibX(), cluster->getCalibY(), cluster->getZ()});
		//cout << "Initial: " << cluster->getCalibE()
		//     << "    " << cluster->getCalibX()
		//     << "    " << cluster->getCalibY()
		//     << "    " << cluster->getZ()
		//     << endl;
	}; 
	for_each(m_clusters.begin(), m_clusters.end(), getExyz);
	px=0.;
	py=0.;
	pz=0.;
	auto calcPxPyPz = [&px,&py,&pz,x0,y0,z0](array<Double_t,4> exyz){
		Double_t dx = exyz[1]-x0;
		Double_t dy = exyz[2]-y0;
		Double_t dz = exyz[3]-z0;
		Double_t EoverL = exyz[0]/sqrt(dx*dx+dy*dy+dz*dz); 
		px += dx*EoverL;
		py += dy*EoverL;
		pz += dz*EoverL;
	};
	for_each(Exyz.begin(), Exyz.end(), calcPxPyPz);	
	//cout << "Final:" << px << "    " << py << "     " << pz << endl;
	//cout << "*******************************************\n";
}
 		

#endif