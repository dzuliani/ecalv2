# CMake generated Testfile for 
# Source directory: /afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple
# Build directory: /afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/Phys/DecayTreeTuple
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(DecayTreeTuple.decaytreetuple-configuration-methods "/cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p6/LbUtils/cmake/xenv" "--xml" "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/config/DaVinciDev-build.xenv" "python" "-m" "GaudiTesting.Run" "--skip-return-code" "77" "--report" "ctest" "--common-tmpdir" "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/Phys/DecayTreeTuple/tests_tmp" "--workdir" "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple/tests/qmtest" "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple/tests/qmtest/decaytreetuple.qms/decaytreetuple-configuration-methods.qmt")
set_tests_properties(DecayTreeTuple.decaytreetuple-configuration-methods PROPERTIES  LABELS "DecayTreeTuple;QMTest" SKIP_RETURN_CODE "77" WORKING_DIRECTORY "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple/tests/qmtest")
