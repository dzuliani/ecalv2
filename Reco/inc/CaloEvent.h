#ifndef ROOT_CaloEvent_h
#define ROOT_CaloEvent_h

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <TClonesArray.h>
#include "Cell.h"
#include "Cluster.h"
#include <TChain.h>
#include <TF2.h>
#include <TRandom3.h>
#include <myParticle.h>
#include <Vertex.h>
#include <vector>
#include <string>
using namespace std;



class CaloEvent: public TObject {

	private:
		ULong64_t m_evt;
		UInt_t    m_run;
		Int_t     m_nregions;
		Double_t  m_z;
		vector<Double_t> m_bordersX;
		vector<Double_t> m_bordersY;
		vector<Double_t> m_cellSizes;
		vector<Double_t> m_molierRadiuses;
		map<Int_t,    Int_t> m_cellMap;
		map<Long64_t, Int_t> m_particleMap;
		map<Int_t,    Int_t> m_clusterMap;
		map<Int_t,    Int_t> m_vertexMap;
		vector<std::pair<Int_t,Int_t>> m_overlapClusters;
		Int_t m_nparticles_rec;
		Int_t m_ncells;
		Int_t m_nclusters;
		Int_t m_nvertices;

		Bool_t m_smearE;
		Int_t  m_smearT;
		Double_t m_alpha;
		Double_t m_beta;
		Double_t m_sigmaT;
		//TClonesArray * m_particles;
		TClonesArray * m_cells;
		TClonesArray * m_clusters;
		TClonesArray * m_vertices;

		map<string,vector<myParticle>> m_containers; 
		//static TClonesArray * m_fg_particles;
		static TClonesArray * m_fg_cells;
		static TClonesArray * m_fg_clusters;
		static TClonesArray * m_fg_vertices;

		static Double_t func(Double_t *var, Double_t *par);
	    static Double_t fracEnergy(Double_t xMin, Double_t xMax, Double_t yMin, Double_t yMax);

		static TRandom3 myRNDM;   
		Double_t getTimeResShape(Double_t E);		
	public:
		Calibrations * calibs;
		CaloEvent();
		virtual ~CaloEvent();
		CaloEvent(vector<Double_t> &xs, vector<Double_t> &ys,
                  vector<Double_t> &sizes, vector<Double_t> mRs,
                  Int_t nregions,Double_t z);
		void Clear(Option_t *option ="");
		void Reset(Option_t *option ="");
		void BuildOld(TChain * genChain, Int_t ev);
		void Build(TChain * genChain, Int_t run, Int_t ev);
		void Build();
		
		void setGeometry(vector<Double_t> &xs, vector<Double_t> &ys,
                     vector<Double_t> &sizes, vector<Double_t> mRs,
                     Int_t nregions,Double_t z);
		void createCells(Int_t region);
		Double_t getEnergyFraction(Cell * cell, myParticle * part);
		void setEvt(ULong64_t evt) { m_evt = evt; }
		ULong64_t getEvt() { return m_evt; }
		void setRun(UInt_t run) { m_run = run; }
		UInt_t getRun() { return m_run; }
		Int_t getNparticles_rec() const {return m_nparticles_rec; }
		Int_t getNcells()   const {return m_ncells; }
		myParticle * AddParticle();
		Cell * AddCell();
		Cluster * AddCluster(Int_t seedID);
		Cluster * AddCluster(Cell *SeedCell);
		Vertex  * AddVertex(Double_t x,Double_t y,Double_t z,Double_t t);
		Vertex  * FindVertex(Double_t x,Double_t y,Double_t z,Double_t t);
		Cluster * getClusterFromID(Int_t id);
		Cell * getCellFromID(Int_t id);
		myParticle * getParticleFromKey(Long64_t key);
		Int_t getIDFromXY(Double_t x, Double_t y);
		void getNeighbours(Int_t cellID, vector<Int_t> &ids);
		void getCellsWithinR(myParticle * part, std::vector<Int_t> &ids, Int_t npos);
		void distributeEnergy(myParticle * part);
    	Double_t getEnergyFraction(myParticle * part, Cell * cell);
    	void doReco(Double_t alpha=0.1, Double_t beta=0.01, Double_t sigmaT=0.0, Double_t factor_Tres=1.);

    	TClonesArray * getCells() const { return m_cells; }
    	//TClonesArray * getParticles() const { return m_particles; }
    	TClonesArray * getClusters() const { return m_clusters; }
    	TClonesArray * getVertices() const { return m_vertices; }

    	Bool_t dumpCells_csv(TString nfout, TString opt, Int_t region, Double_t E_th=1);
   		vector<TGraphErrors*> TimeShape{nullptr, nullptr, nullptr};

   		void setSeeds_LocalMaxima(Double_t Eth=50);  //1
   		void setSeeds_Correct2Seed(Double_t Eth=50); //2
   		void setSeeds_TruePhotonFromPi0(Double_t Eth=0); //3
		void setSeeds_2Seed(Double_t Eth=50); // 4 (1seed), 5 (2seed)
		void turnOff2Seeds_mergedPi0();
		vector<Cell*> m_SeedCells;
		

		void makeClusters(Int_t iterations=5);
		void clearClusters();
		void findOverlappingClusters();
		void energy_redistribution_pro(Int_t ID_I, Int_t ID_J, Int_t iterations);

		vector<TH2D> monitor(TString nfout="none", TString option="RECREATE");
		void addContainerReconstructed(TString ncontainer);
		void addContainers(map<string,Int_t>containers, TTree* input, Bool_t checkAcc=false);
		vector<myParticle>* getContainer(TString ncontainer) { return &(m_containers[ncontainer.Data()]); }
		void linkRelativesParts(map<string,Int_t>containers);
		void setGDparents();
		void sortDaughters();
		void combineParticles_resolvPi0(TString ncontainer);
		void combineParticles_mergedPi0(TString ncontainer);
		void matchParticles_Pi0(TString ncontainer_TRUE, TString ncontainer_REC);
		void combineParticles_Bd3pi(TString ncont_Bd, TString ncont_pip, TString ncont_pim, TString ncont_pi0);
		void matchParticles_Bd3pi(TString ncontainer_REC);
		void flagSig_Bd3pi(TString ncont_Bd1,TString ncont_Bd2, TChain * genChain);
		void flagPi0(TString ncont_pi0);
		Cell* find2Seed_mergedPi0(Cell* seed1);
		ClassDef(CaloEvent,1)
};


#endif
