#ifndef ROOT_Cell_h
#define ROOT_Cell_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <TRefArray.h>
#include <TGraphErrors.h>
#include <TProfile.h>
#include <TH1D.h>
#include <myProtoParticle.h>
#include "sigShape.h"

using namespace std;

struct PhContrib {
	Double_t m_e;
	Double_t m_frac;
	Double_t m_t;
};

struct Calibrations {
	vector<TGraphErrors> g_shapes;
	vector<Double_t> TimeDelay;
	vector<Double_t> Ecalib;
	vector<TProfile> Xcalib;
	vector<TProfile> Ycalib;
	Calibrations(){};
	Calibrations(TString nfin, std::map<TString, TString>setup);
};

class Cell: public TObject {

	private:
		Double_t m_x;
		Double_t m_y;
		Double_t m_z;
		Double_t m_size;
		Double_t m_e;
		Double_t m_t;
		Int_t m_id;
		Int_t m_col;
		Int_t m_row;
		Int_t m_region;
		Int_t m_nphotons;
		
		Int_t m_isSeed;
		vector<Int_t> m_neighbours;
		vector<Int_t> m_nearSeeds;
	public:
		Cell() {initialise();};
		Cell(Double_t x, Double_t y, Int_t reg);
		virtual ~Cell() {};
		vector<myProtoParticle*> m_photons;
		map<Long64_t,PhContrib> m_mapPhotons;
    	void initialise();
		void setX(Double_t x) { m_x = x; }
		void setY(Double_t x) { m_y = x; }
		void setZ(Double_t z) { m_z = z; }
		void setT(Double_t t) { m_t = t; }
		void setSize(Double_t s) { m_size = s; }
		void setRegion(Int_t reg) { m_region = reg; }
		void setID(Int_t col, Int_t row, Int_t region) { 
			m_col = col; 
			m_row = row;
			m_region = region;
			m_id = (col << 17) + (row << 2) + region;
		}
  		void setNeighbours(vector<Int_t> neighbours){m_neighbours=neighbours;}
  		vector<int, std::allocator<int> >::iterator getNeigBegin() { return m_neighbours.begin();}
  		vector<int, std::allocator<int> >::iterator getNeigEnd() { return m_neighbours.end();}
  		vector<Int_t> getNeighbours(){ return m_neighbours;}
  		
  		void addNearSeed(Int_t seedID) {m_nearSeeds.push_back(seedID);}
  		vector<Int_t> getNearSeedsID() { return m_nearSeeds; }
  		Int_t getNearSeedsN() { return m_nearSeeds.size(); }
  		void removeNearSeed(Int_t v);
  		void setSeedType(Int_t seedType){ m_isSeed = seedType;}
		Double_t getX1()  { return m_x - 0.5*m_size; }
		Double_t getX2()  { return m_x + 0.5*m_size; }
		Double_t getY1()  { return m_y - 0.5*m_size; }
		Double_t getY2()  { return m_y + 0.5*m_size; }
		Double_t getX()   { return m_x; }
		Double_t getY()   { return m_y; }
		Double_t getZ()   { return m_z; }
		Double_t getSize() { return m_size; }
		Double_t getDistance(Double_t x, Double_t y);
    	Double_t getE();
    	Double_t getEtrue();
		Int_t getCol()    { return ((m_id >> 17) & 0x7FFF); }
		Int_t getRow()    { return ((m_id >> 2) & 0x7FFF); }
		Int_t getRegion() { return (m_id & 3); }
		void addPhoton(myProtoParticle* photon, Double_t frac);
		void addPhoton(myProtoParticle* photon, Double_t frac, Double_t e, Double_t t);
		Int_t getNphotons() { return m_nphotons; }
    
		//TRefArray &getPhotons() { return m_photons; }
    	vector<myProtoParticle*> getPhotons() { return m_photons; }
    	Double_t getPhotonE(myProtoParticle* photon);
    	Double_t getPhotonT(myProtoParticle* photon);
    	Double_t getPhotonFrac(myProtoParticle* photon);
		Int_t getID() { return m_id; }

		vector<myProtoParticle*> getHittingPhotons(Double_t E_th=1);
		Double_t getT() { return m_t; }
		Double_t getT_Eavg();
		Double_t getT_Eavg_true();
		void smearTimeFromShape(Calibrations *calibs, Double_t factor_Tres=1.);
		Int_t getSeedType(){ return m_isSeed; }
		Double_t getMaxDepositTime();

		ClassDef(Cell,1)
};



#endif
