#ifndef Cluster_cxx
#define Cluster_cxx


#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <algorithm>
#include <numeric>
#include <Cluster.h>
#include <TLeaf.h>
#include <TFile.h>
#include <TF2.h>
using namespace std;

ClassImp(myProtoParticle )
ClassImp(Cell)

Cluster::Cluster(){
	m_vec_cellIDs.reserve(9);
	m_vec_e.reserve(9);
	m_ncells = 0;
	m_RecParticle = 0;
}
Cluster::Cluster(Cell *SeedCell){
	m_vec_cellIDs.reserve(9);
	m_vec_e.reserve(9);
	m_ncells = 0;
	m_ID = SeedCell->getID();
	m_z  = SeedCell->getZ();
	m_RecParticle = 0;
	this->addCell(SeedCell);
}

void Cluster::addCell(Cell *cell){
	//cout << "*** "<< cell <<"\n";
	m_vec_cellIDs.push_back(cell->getID());
	m_vec_e.push_back(cell->getE());
	m_cells.push_back(cell);
	++m_ncells;
	m_cellMap[cell->getID()] = m_ncells-1;
}

Cell * Cluster::getSeed(){	
	Cell * cell = m_cells[0];
	if (cell->getID() == m_ID) return cell;
	else return nullptr;
}

void Cluster::calibrate(Calibrations * calibs){
	Int_t region_i = getSeedRegion()-1;
	m_calib_e = m_e/(1+calibs->Ecalib[region_i]);
	Cell * seedCell = getSeed();
	Double_t seed_x = seedCell->getX();
	Double_t seed_y = seedCell->getY();
	m_calib_x = seed_x + calibs->Xcalib[region_i].Interpolate(m_x-seed_x);
	m_calib_y = seed_y + calibs->Ycalib[region_i].Interpolate(m_y-seed_y);
}
	
void Cluster::finalize(Calibrations * calibs){
	m_e = accumulate(m_vec_e.begin(), m_vec_e.end(),0.0);
	m_x = 0.0;
	m_y = 0.0;
	for (Int_t i=0; i<m_ncells; ++i){
		Cell *cell = (Cell*)m_cells[i];
		m_x += m_vec_e[i]*cell->getX();
		m_y += m_vec_e[i]*cell->getY();
		m_t += m_vec_e[i]*cell->getT();
	}
	m_x = m_x/m_e;
	m_y = m_y/m_e;
	m_t = m_t/m_e;
	if (calibs != 0) 
		calibrate(calibs);
}

Cell * Cluster::getCellFromID(Int_t id) {
   if(id <= 0) {
      //printf("ID OUT OF RANGE\n");
      return NULL;
   }
   Int_t pos = m_cellMap.find(id)->second;
   return m_cells.at(pos);
}
void Cluster::setE_fromID(Int_t id, Double_t value){
	Int_t pos = m_cellMap.find(id)->second;
	m_vec_e[pos] = value;
}

vector<myProtoParticle*> Cluster::getHittingPhotons(Double_t Eth){
	vector<myProtoParticle*> hittingPhotons;
	//cout << "Cluster::getHittingPhotons, # cells :" <<  m_cells.size() << endl;
	for (auto it_cell = m_cells.begin(); it_cell != m_cells.end(); ++it_cell){
	//	cout << ".";
		Cell * cell = (Cell*)(*it_cell);
		auto tmp_hitingPhotons = cell->getHittingPhotons(Eth);
	//	cout << cell->getID() << "  N hits = " << tmp_hitingPhotons.size() << endl;
		hittingPhotons.insert(hittingPhotons.end(), tmp_hitingPhotons.begin(), tmp_hitingPhotons.end());
	}
	//cout << "\nCluster::getHittingPhotons :" <<  hittingPhotons.size() << endl;
	return hittingPhotons;
}
Int_t Cluster::getNphotons(){
	vector<myProtoParticle*> hittingPhotons;
	Int_t Nphotons = 0;
	for (auto it_cell = m_cells.begin(); it_cell != m_cells.end(); ++it_cell){
		Cell * cell = (Cell*)(*it_cell);
		Nphotons += cell->getNphotons();
	}
	return Nphotons;
}

myProtoParticle* Cluster::getCloserPhoton(Double_t Eth){
	vector<Long64_t> hitIDs;
	hitIDs.reserve(5);
	Double_t dist=9999999., tmp_dist=0., tmp_photonX=0.,tmp_photonY=0.;
	myProtoParticle * closerPhoton = 0;
	auto hitPhotons = this->getHittingPhotons(Eth);
	for (auto& hitPhoton : hitPhotons){
		if (find(hitIDs.begin(),hitIDs.end(), hitPhoton->getKey()) != hitIDs.end()) continue;
		hitIDs.push_back(hitPhoton->getKey());
		//hitPhoton->getXYatZ(tmp_photonX, tmp_photonY,m_z);
		tmp_photonX = hitPhoton->getCaloVertexX();
		tmp_photonY = hitPhoton->getCaloVertexY();

		tmp_dist = (tmp_photonX-m_calib_x)*(tmp_photonX-m_calib_x) + (tmp_photonY-m_calib_y)*(tmp_photonY-m_calib_y);
		if (tmp_dist > dist) continue;
		dist = tmp_dist;
		closerPhoton = hitPhoton; 
	}
	//if (closerPhoton == 0){
	//	cout << "WARNING: Cluster::getCloserPhoton -> no photon found!!\n";
	//	cout.flush();
	//}
	return closerPhoton;
}

Double_t Cluster::getPx(){
	//cout << " fanculooooo:  "<< m_e << "    " << m_calib_e << "\n";
	return m_calib_e*m_calib_x / sqrt(m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}
Double_t Cluster::getPy(){
	return m_calib_e*m_calib_y / sqrt(m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}
Double_t Cluster::getPz(){
	return m_calib_e*m_z / sqrt(m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}
Double_t Cluster::getPt(){
	return sqrt(getPt2());
}
Double_t Cluster::getPt2(){
	return      m_calib_e*m_calib_e*(m_calib_x*m_calib_x+m_calib_y*m_calib_y) / (m_calib_x*m_calib_x+m_calib_y*m_calib_y+m_z*m_z);
}

#endif