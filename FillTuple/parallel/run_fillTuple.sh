#!/bin/bash

. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh
Nsubjob=$1
Nevt_perJob=$2
Tag=$3
fullSim=$4

nu=0
simType=0

if [ $fullSim -eq 1 ]; 
then simType=full;
elif [ $fullSim -eq 0 ];
then simType=stand;
fi

if [[ $Tag == *"upgrade2"* ]];
then nu=71;
elif [[ $Tag == *"upgrade0"* ]];
then nu=2;
fi


pathOutput=$ProjectDir/storage/"$simType"_Bd_3pi_nu"$nu"_20200106/evtType_11102404/$Nsubjob/$Tag
echo $pathOutput
echo 'Nsubjob:  '$Nsubjob

#$ProjectDir/FillTuple/parallel/fillTuple $Nsubjob $Nevt_perJob $Tag $fullSim 
#$ProjectDir/FillTuple/parallel/fillTuplePi0       $pathOutput/fillTuple.root $pathOutput/outTuplePi0.root
$ProjectDir/FillTuple/parallel/fillTupleBd3pi     $pathOutput/fillTuple.root $pathOutput/outTupleBd3pi.root
#ProjectDir/FillTuple/parallel/fillTuplePi0True   $pathOutput/fillTuple.root $pathOutput/outTuplePi0True.root
$ProjectDir/FillTuple/parallel/fillTupleBd3piTrue $pathOutput/fillTuple.root $pathOutput/outTupleBd3piTrue.root
#$ProjectDir/FillTuple/parallel/fillTupleGamma      $pathOutput/fillTuple.root $pathOutput/outTupleGamma.root
