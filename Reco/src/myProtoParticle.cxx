
#ifndef ROOT_myProtoParticle_cxx
#define ROOT_myProtoParticle_cxx

#include <myProtoParticle.h>
myProtoParticle::myProtoParticle(){
	m_mother        = 0;
	m_gd_mother     = 0;
	m_daughter.clear();
	m_gd_daughter.clear();
	
	m_key           = -99999;
	m_mother_key    = -99999;
	m_gd_mother_key = -99999;
	m_id            = -99999;
	m_mother_id     = -99999;
	m_gd_mother_id  = -99999;
	m_OV            = 0;
	m_CALOV         = 0;
	m_ENDV          = 0;
	m_orig_px       = 0.;
	m_orig_py       = 0.;
	m_orig_pz       = 0.;
	m_orig_e        = 0.;
	m_calo_px       = 0.;
	m_calo_py       = 0.;
	m_calo_pz       = 0.;
	m_calo_e        = 0.;

}
myProtoParticle::myProtoParticle(TLorentzVector &mom, Double_t mass) {
	m_mother        = 0;
	m_gd_mother     = 0;
	m_daughter.clear();
	m_gd_daughter.clear();
	m_key           = -99999;
	m_mother_key    = -99999;
	m_gd_mother_key = -99999;
	m_id            = -99999;
	m_mother_id     = -99999;
	m_gd_mother_id  = -99999;
	m_OV            = 0;
	m_CALOV         = 0;
	m_ENDV          = 0;
	m_orig_px       = mom.Px();
	m_orig_py       = mom.Py();
	m_orig_pz       = mom.Pz();
	m_orig_e        = mom.E();
	m_calo_px       = 0.;
	m_calo_py       = 0.;
	m_calo_pz       = 0.;
	m_calo_e        = 0.;
	m_mass = mass;
}

void myProtoParticle::setOrigMom(Double_t x, Double_t y, Double_t z) {
	m_orig_px = x; m_orig_py = y; m_orig_pz = z;
}
void myProtoParticle::setCaloMom(Double_t x, Double_t y, Double_t z) {
	m_calo_px = x; m_calo_py = y; m_calo_pz = z;
}

void myProtoParticle::getXYatZ(Double_t &x, Double_t &y, Double_t z) {
	x = m_OV->getX() + m_orig_px/m_orig_pz*(z-m_OV->getZ());
	y = m_OV->getY() + m_orig_py/m_orig_pz*(z-m_OV->getZ());
}

Double_t myProtoParticle::getTatZ(Double_t z) {
	Double_t x0 = m_OV->getX();
	Double_t y0 = m_OV->getY();
	Double_t z0 = m_OV->getZ();
	Double_t t0 = m_OV->getT();

	Double_t x = 0, y = 0;
	getXYatZ(x,y,z);
	Double_t T = (x-x0)*(x-x0)+(y-y0)*(y-y0)+(z-z0)*(z-z0);
	T = sqrt(T)/0.299792458;
	return t0+T;
}

void myProtoParticle::setKey(Long64_t runNumber, Long64_t evtNumber, Int_t tmpKey){
	m_key = Long64_t( Long64_t(runNumber<<48) | Long64_t(evtNumber<<32) |  Long64_t(tmpKey) );
}

void myProtoParticle::setMkey(Long64_t runNumber, Long64_t evtNumber, Int_t tmpKey){
	m_mother_key = Long64_t( Long64_t(runNumber<<48) |  Long64_t(evtNumber<<32) |  Long64_t(tmpKey));
}

void myProtoParticle::setGmkey(Long64_t runNumber, Long64_t evtNumber, Int_t tmpKey){
	m_gd_mother_key = Long64_t( Long64_t(runNumber<<48) |  Long64_t(evtNumber<<32) |  Long64_t(tmpKey));
}
void myProtoParticle::sort_daughters(){
	auto f_order = [](myProtoParticle* part1, myProtoParticle* part2){
		auto id1 = part1->getID();
		auto id2 = part2->getID();
		if (abs(id1) != abs(id2))  return abs(id1) > abs(id2);
		if (id1 != id2)            return id1 > id2;
		return part1->getKey() > part2->getKey();
	};
	sort(m_daughter.begin(), m_daughter.end(), f_order);
}
void myProtoParticle::sort_gd_daughters(){
	auto f_order = [](myProtoParticle* part1, myProtoParticle* part2){
		auto id1 = part1->getID();
		auto id2 = part2->getID();
		if (abs(id1) != abs(id2))  return abs(id1) > abs(id2);
		if (id1 != id2)            return id1 > id2;
		return part1->getKey() > part2->getKey();
	};
	sort(m_gd_daughter.begin(), m_gd_daughter.end(), f_order);
}
    	
 		

#endif