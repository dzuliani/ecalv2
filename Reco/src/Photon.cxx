#ifndef Photon_cxx
#define Photon_cxx

#include <Photon.h>

Photon::Photon(TVector3 &pos, TVector3 &mom, Double_t T) {

	m_x = pos.X(); m_y = pos.Y(); m_z = pos.Z();
	m_px = mom.X(); m_py = mom.Y(); m_pz = mom.Z();
	m_t = T;

}

void Photon::setPosT(Double_t x, Double_t y, Double_t z, Double_t T) {

	m_x = x; m_y = y; m_z = z;
	m_t = T;

}

void Photon::setMom(Double_t x, Double_t y, Double_t z) {

	m_px = x; m_py = y; m_pz = z;

}

void Photon::getXYatZ(Double_t &x, Double_t &y, Double_t z) {

	x = m_x + m_px/m_pz*(z-m_z);
	y = m_y + m_py/m_pz*(z-m_z);
}

Double_t Photon::getTatZ(Double_t z) {

	Double_t x = 0, y = 0;
	getXYatZ(x,y,z);
	Double_t T = (x-m_x)*(x-m_x)+(y-m_y)*(y-m_y)+(z-m_z)*(z-m_z);
	T = sqrt(T)/0.299792458;
	return m_t+T;
}
void Photon::setKey(ULong64_t evtNumber, Int_t tmpKey){
	m_key = Long64_t( Long64_t(evtNumber<<32) |  Long64_t(tmpKey));
}
void Photon::setMkey(ULong64_t evtNumber, Int_t tmpKey){
	m_mother_key = Long64_t( Long64_t(evtNumber<<32) |  Long64_t(tmpKey));
}
void Photon::setGmkey(ULong64_t evtNumber, Int_t tmpKey){
	m_gd_mother_key = Long64_t( Long64_t(evtNumber<<32) |  Long64_t(tmpKey));
}

#endif