#ifndef ROOT_Vertex_h
#define ROOT_Vertex_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <array>
#include <TObject.h>
using namespace std;

class Vertex: public TObject {
private:
	Long64_t m_ID;
	Double_t m_x;
	Double_t m_y;
	Double_t m_z;
	Double_t m_t;
public:
	Vertex(){ initialise(); }
	Vertex(Double_t x,Double_t y,Double_t z,Double_t t);
	void initialise();
	void setID(Long64_t v){ m_ID= v; }
	void setID(UInt_t run, ULong64_t evt, Long64_t v);
	void setX(Double_t v) { m_x = v; }
	void setY(Double_t v) { m_y = v; }
	void setZ(Double_t v) { m_z = v; }
	void setT(Double_t v) { m_t = v; }
	Int_t    getID(){ return m_ID;}
	Double_t getX() { return m_x; }
	Double_t getY() { return m_y; }
	Double_t getZ() { return m_z; }
	Double_t getT() { return m_t; }
	array<Double_t,3> getPos();
	array<Double_t,4> getPosT();
	
	ClassDef(Vertex,1)
};

#endif