# Install script for directory: /afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/InstallArea/x86_64-centos7-gcc8-opt")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "0")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python/DecayTreeTuple" TYPE FILE OPTIONAL FILES
    "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/Phys/DecayTreeTuple/genConf/DecayTreeTuple/DecayTreeTupleConf.py"
    "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/Phys/DecayTreeTuple/genConf/DecayTreeTuple/__init__.py"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE MODULE OPTIONAL FILES "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/lib/libDecayTreeTuple.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so")
    file(RPATH_CHANGE
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so"
         OLD_RPATH "/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/HepMC/2.06.10/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r6/InstallArea/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/clhep/2.4.1.2/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/GSL/2.5/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/vdt/0.4.3/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/Python/2.7.16/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r6/InstallArea/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r6/InstallArea/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lcg/releases/LCG_96b/XercesC/3.1.3/x86_64-centos7-gcc8-opt/lib:/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v30r6/InstallArea/x86_64-centos7-gcc8-opt/lib:/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v32r2/InstallArea/x86_64-centos7-gcc8-opt/lib:"
         NEW_RPATH "")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libDecayTreeTuple.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/python" TYPE DIRECTORY FILES "/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple/python/" FILES_MATCHING REGEX "/[^/]*\\.py$" REGEX "/CVS$" EXCLUDE REGEX "/\\.svn$" EXCLUDE)
endif()

