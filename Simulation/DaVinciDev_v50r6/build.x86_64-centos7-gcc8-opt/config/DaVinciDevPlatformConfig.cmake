# File automatically generated: DO NOT EDIT.

# Get the exported informations about the targets
get_filename_component(_dir "${CMAKE_CURRENT_LIST_FILE}" PATH)

# Set useful properties
get_filename_component(_dir "${_dir}" PATH)
set(DaVinciDev_INCLUDE_DIRS ${_dir}/include)
set(DaVinciDev_LIBRARY_DIRS ${_dir}/lib)

set(DaVinciDev_BINARY_PATH ${_dir}/bin ${_dir}/scripts)
set(DaVinciDev_PYTHON_PATH ${_dir}/python)

set(DaVinciDev_COMPONENT_LIBRARIES DecayTreeTupleBase;DecayTreeTupleMC;DecayTreeTuple)
set(DaVinciDev_LINKER_LIBRARIES DecayTreeTupleBaseLib)

set(DaVinciDev_ENVIRONMENT PREPEND;PYTHONPATH;\${LCG_releases_base}/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc8-opt/lib;PREPEND;PATH;/cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/bin;PREPEND;PATH;\${LCG_releases_base}/LCG_96b/Python/2.7.16/x86_64-centos7-gcc8-opt/bin;PREPEND;PATH;\${LCG_releases_base}/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc8-opt/bin;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/vdt/0.4.3/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/tbb/2019_U7/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/GSL/2.5/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/clhep/2.4.1.2/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/Python/2.7.16/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/HepMC/2.06.10/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;\${LCG_releases_base}/LCG_96b/XercesC/3.1.3/x86_64-centos7-gcc8-opt/lib;PREPEND;LD_LIBRARY_PATH;/cvmfs/sft.cern.ch/lcg/releases/gcc/8.3.0-cebb0/x86_64-centos7/lib64;SET;ROOTSYS;\${LCG_releases_base}/LCG_96b/ROOT/6.18.04/x86_64-centos7-gcc8-opt;SET;PYTHONHOME;\${LCG_releases_base}/LCG_96b/Python/2.7.16/x86_64-centos7-gcc8-opt;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/GAUDI/GAUDI_v32r2/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/LHCB/LHCB_v50r6/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/LBCOM/LBCOM_v30r6/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/REC/REC_v30r6/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/PHYS/PHYS_v30r6/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/STRIPPING/STRIPPING_v15r1/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/ANALYSIS/ANALYSIS_v30r6/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;/cvmfs/lhcb.cern.ch/lib/lhcb/DAVINCI/DAVINCI_v50r6/InstallArea/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;\${LCG_releases_base}/LCG_96b/Boost/1.70.0/x86_64-centos7-gcc8-opt/include;PREPEND;ROOT_INCLUDE_PATH;\${LCG_releases_base}/LCG_96b/HepMC/2.06.10/x86_64-centos7-gcc8-opt/include;SET;GAUDIAPPNAME;DaVinciDev;SET;GAUDIAPPVERSION;v50r6;PREPEND;PATH;\${.}/scripts;PREPEND;PATH;\${.}/bin;PREPEND;LD_LIBRARY_PATH;\${.}/lib;PREPEND;ROOT_INCLUDE_PATH;\${.}/include;PREPEND;PYTHONPATH;\${.}/python;PREPEND;PYTHONPATH;\${.}/python/lib-dynload;SET;DAVINCIDEV_PROJECT_ROOT;\${.}/../../;SET;DECAYTREETUPLEBASEROOT;\${DAVINCIDEV_PROJECT_ROOT}/Phys/DecayTreeTupleBase;SET;DECAYTREETUPLEMCROOT;\${DAVINCIDEV_PROJECT_ROOT}/Phys/DecayTreeTupleMC;SET;DECAYTREETUPLEROOT;\${DAVINCIDEV_PROJECT_ROOT}/Phys/DecayTreeTuple)

set(DaVinciDev_EXPORTED_SUBDIRS)
foreach(p Phys/DecayTreeTupleBase;Phys/DecayTreeTupleMC;Phys/DecayTreeTuple)
  get_filename_component(pn ${p} NAME)
  if(EXISTS ${_dir}/cmake/${pn}Export.cmake)
    set(DaVinciDev_EXPORTED_SUBDIRS ${DaVinciDev_EXPORTED_SUBDIRS} ${p})
  endif()
endforeach()

set(DaVinciDev_OVERRIDDEN_SUBDIRS )
