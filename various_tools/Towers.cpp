#ifndef Towers_cpp
#define Towers_cpp
//c++
#include <tuple>
#include <map>
#include <string>
#include <stdlib.h>
#include <fstream>


#include "Towers.h"

Towers::Towers(std::map <Long64_t,SimpleCell> Cells_Container, Int_t evtNumber, ECal_Geo *ECal){
	m_Cells_Container = Cells_Container;
	m_evtNumber = evtNumber;
	//MyECal->set_Geometry(ECal.z(),ECal.get_xBorder(), ECal.get_yBorder(), ECal.get_xBins(), ECal.get_yBins(), ECal.get_RegionNames(), ECal.get_nfileCalib());
	MyECal = ECal;
}
void Towers::write(std::ofstream &os){
	Int_t len = m_Cells_Container.size();
	os.write((char*)&len, sizeof(len));
	for (auto p : m_Cells_Container){
		os.write((char*)&p.first, sizeof(p.first));
		p.second.write(os);
	}
	os.write((char*)&m_evtNumber, sizeof(m_evtNumber));
}
void Towers::read(std::ifstream &is){
	Int_t len = 0;
	is.read((char*)&len, sizeof(len));
	m_Cells_Container.clear();
	for (Int_t  i=0; i<len; ++i){
		Long64_t ID;
		SimpleCell cell;
		is.read((char*)&ID, sizeof(ID));
		cell.read(is);
		std::pair<Long64_t, SimpleCell> p(ID, cell);
		m_Cells_Container.insert(p);
	}
	is.read((char*)&m_evtNumber, sizeof(m_evtNumber));
}
std::vector<TH2D> Towers::monitor(TString nval = "energy", TString drawOpt = "COL", TString nfout="none"){  
	gStyle->SetOptStat(0);
	TH2F* blank = new TH2F("blank","blank",1,-1*((MyECal->get_xBorder())[0]),(MyECal->get_xBorder())[0],
																				 1,-1*((MyECal->get_yBorder())[0]),(MyECal->get_yBorder())[0]);
	blank->SetFillColor(kWhite);
	TH2F* inner = new TH2F("inner","inner",(MyECal->get_tot_xBins()->at("Inner")).size()-1,-1*(MyECal->get_xBorder())[1], (MyECal->get_xBorder())[1],
            	                           (MyECal->get_tot_yBins()->at("Inner")).size()-1,-1*(MyECal->get_yBorder())[1], (MyECal->get_yBorder())[1]);
  	TH2F* middle = new TH2F("middle","middle",(MyECal->get_tot_xBins()->at("Middle")).size()-1,-1*(MyECal->get_xBorder())[2], (MyECal->get_xBorder())[2],
        	                                  (MyECal->get_tot_yBins()->at("Middle")).size()-1,-1*(MyECal->get_yBorder())[2], (MyECal->get_yBorder())[2]);
  	TH2F* outer = new TH2F("outer","outer",(MyECal->get_tot_xBins()->at("Outer")).size()-1,-1*(MyECal->get_xBorder())[3], (MyECal->get_xBorder())[3],
    	                                   (MyECal->get_tot_yBins()->at("Outer")).size()-1,-1*(MyECal->get_yBorder())[3], (MyECal->get_yBorder())[3]);  
  	for(auto &cell : m_Cells_Container){
    	Int_t region = MyECal->get_RegionID(cell.first);
		Int_t xID = MyECal->get_xID(cell.first);
		Int_t yID = MyECal->get_yID(cell.first);
		Double_t val = 0.;
		if (nval == "energy")
			val = cell.second.Energy();
		else if (nval == "seed")
			val = (Double_t)((Int_t)cell.second.get_seed());
		else if (nval == "time")
			val = cell.second.get_time();
    	if(region == 1)
      		inner->SetBinContent(xID, yID, val); 
    	else if(region == 2)
    	  	middle->SetBinContent(xID, yID, val); 
    	else if(region == 3)
      	outer->SetBinContent(xID, yID, val); 
  	}
	Double_t max(0.),min(0.);
	Int_t binMax0 = inner->GetMaximumBin();
	Int_t binMax1 = middle->GetMaximumBin();
	Int_t binMax2 = outer ->GetMaximumBin();
	max = inner->GetBinContent(binMax0);
	(max<middle->GetBinContent(binMax1))? max = middle->GetBinContent(binMax1) : max=max;
	(max<outer ->GetBinContent(binMax2))? max = outer ->GetBinContent(binMax2) : max=max;
	//if (nval == "energy" && max>50000) max = 50000;
	if (nval == "seed") max = 5;
	inner->SetMaximum(max);
	middle->SetMaximum(max);
	outer->SetMaximum(max);

	Int_t binmin0 = inner->GetMinimumBin();
	Int_t binmin1 = middle->GetMinimumBin();
	Int_t binmin2 = outer ->GetMinimumBin();
	min = inner->GetBinContent(binmin0);
	(min>middle->GetBinContent(binmin1))? min = middle->GetBinContent(binmin1) : min=min;
	(min>outer ->GetBinContent(binmin2))? min = outer ->GetBinContent(binmin2) : min=min;
	if (nval == "energy"){ min= 1.;}
	else if (nval == "seed"){min = 0.;}
	else if (nval == "time"){min = max-2.5;}
	inner->SetMinimum(min);
	middle->SetMinimum(min);
	outer->SetMinimum(min);
	
	

	TCanvas *c = new TCanvas("c","c",1200,900);
	outer->Draw(drawOpt);
	middle->Draw(drawOpt+" same");
  	inner->Draw(drawOpt+" same");
	//std::vector<TLine> grid = getGrid();
	//for (auto line : grid)
	//	line.Draw("same");
	std::vector<TH2D> houts;
	houts.push_back(*(TH2D*)inner->Clone("innerRET"));
	houts.push_back(*(TH2D*)middle->Clone("middleRET"));
	houts.push_back(*(TH2D*)outer->Clone("outerRET"));

	blank->Draw(drawOpt+" same");
  	if (nfout != "none"){
		c->SaveAs(nfout+".pdf"); 
		TFile f(nfout+".root","recreate"); 
		f.WriteTObject(c, c->GetName(), "overwrite");
  		f.WriteTObject(inner,  inner->GetName(),  "overwrite");
  		f.WriteTObject(middle, middle->GetName(), "overwrite");
  		f.WriteTObject(outer,  outer->GetName(),  "overwrite");
		f.Close();
	}
  	delete blank;
  	delete inner;
  	delete middle;
  	delete outer;
	delete c;
	return houts;
}

void Towers::print(std::ostream &os){
	Int_t i=0;
	for (auto p : m_Cells_Container){
		os << "==================== CELL "<< i << " ========================\n";
		p.second.print(os);
		++i;
	}
	os << "================== ECAL Geo =======================\n";
	MyECal->print(os);
}


void Towers::add(Towers &T){
	for (auto new_cell_container : T.getTowers()){ //<-- loop on the cells to add
		if (m_Cells_Container.find(new_cell_container.first) == m_Cells_Container.end()) {
			m_Cells_Container.insert(new_cell_container);
		} else {
			SimpleCell *this_cell = &(m_Cells_Container.find(new_cell_container.first)->second);
			this_cell->add(new_cell_container.second);
		}
	}
}

void Towers::reco(Double_t alpha, Double_t beta, Double_t Time_Smear, Double_t threshold, Bool_t mantainTRUEseeds){
	std::map<Long64_t,SimpleCell>::iterator it = m_Cells_Container.begin();
	while (it != m_Cells_Container.end()){
		it->second.reco(alpha, beta, Time_Smear, mantainTRUEseeds);	
		if ( it->second.Energy() < threshold){
			Long64_t key = it->first;
			++it;
			m_Cells_Container.erase(key);
			continue;
		}
		++it;
	}
}

void Towers::recoTimeShape(Double_t alpha, Double_t beta, Double_t threshold, Bool_t mantainTRUEseeds){
	std::map<Long64_t,SimpleCell>::iterator it = m_Cells_Container.begin();
	while (it != m_Cells_Container.end()){
		it->second.recoTimeShape(alpha, beta, MyECal, mantainTRUEseeds);	
		if ( it->second.Energy() < threshold){
			Long64_t key = it->first;
			++it;
			m_Cells_Container.erase(key);
			continue;
		}
		++it;
	}
}






















ECALinterp::ECALinterp() : Towers() {
	cut_mergedPi0_d_gammas_min = 2.38;
	cut_mergedPi0_M_min =  95;
	cut_mergedPi0_M_max = 215;
	resetInterpContainers();
}
ECALinterp::ECALinterp(Towers &T) : Towers(T.getTowers(), T.m_evtNumber, T.MyECal){
	cut_mergedPi0_d_gammas_min = 2.38;
	cut_mergedPi0_M_min =  95;
	cut_mergedPi0_M_max = 215;
	resetInterpContainers();
}
ECALinterp::ECALinterp(std::map <Long64_t,SimpleCell> Cells_Container, Int_t evtNumber) : Towers(Cells_Container, evtNumber) {
	cut_mergedPi0_d_gammas_min = 2.38;
	cut_mergedPi0_M_min =  95;
	cut_mergedPi0_M_max = 215;
	resetInterpContainers();
}
ECALinterp::ECALinterp(std::map <Long64_t,SimpleCell> Cells_Container, Int_t evtNumber, ECal_Geo *ECal) : Towers (Cells_Container, evtNumber, ECal) {
	cut_mergedPi0_d_gammas_min = 2.38;
	cut_mergedPi0_M_min =  95;
	cut_mergedPi0_M_max = 215;
	resetInterpContainers();
};
	







void ECALinterp::findSeeds0(Double_t E_T_th){
	// Cellular Autom Alg.
	m_vec_seeds.clear();
	for (auto cell_container : m_Cells_Container)
		cell_container.second.reset_near_seeds();

	for (auto cell_container : m_Cells_Container){        // loop over all cell that got energy
		Double_t E = cell_container.second.Energy();
		Double_t x = (cell_container.second.x1()+cell_container.second.x2())/2.0;
		Double_t y = (cell_container.second.y1()+cell_container.second.y2())/2.0;
		Double_t z = cell_container.second.z();
		Double_t T = TMath::Sqrt(x*x+y*y);
		Double_t E_T=E*T/TMath::Sqrt(T*T+z*z);
		if (E_T<E_T_th) continue;                             // if E_T<E_th, this is not a seed: continue
		Long64_t ID= cell_container.first;
		std::vector<Long64_t> vec_nearby_0;
		std::vector<Long64_t> vec_nearby;
		MyECal->get_nearby(ID, vec_nearby_0);
		
		Bool_t tmp_seed = true;
		for (auto near_ID: vec_nearby_0){
			if (m_Cells_Container.find(near_ID) == m_Cells_Container.end()) continue;
			vec_nearby.push_back(near_ID);
			if (m_Cells_Container[near_ID].Energy() > E){
				tmp_seed = false;
				break;
			}
		}
		m_Cells_Container[ID].set_near(vec_nearby);
		m_Cells_Container[ID].set_seed(tmp_seed);
		if (tmp_seed){
			m_Cells_Container[ID].Add_near_seed(ID);
			m_vec_seeds.push_back(ID);
			for (auto cell_near_ID : vec_nearby)
				m_Cells_Container[cell_near_ID].Add_near_seed(ID);
		}
	}
}

void ECALinterp::update_vec_seeds(){
	//m_vec_seeds.clear();
	std::vector<Long64_t> new_vec_seeds;
	for (auto cell_container : m_Cells_Container){
		if (cell_container.second.get_seed()) 
			new_vec_seeds.push_back(cell_container.first);
	}
	std::sort(new_vec_seeds.begin(), new_vec_seeds.end());
	auto it = std::unique(new_vec_seeds.begin(), new_vec_seeds.end());
	new_vec_seeds.resize(std::distance(new_vec_seeds.begin(),it));
	m_vec_seeds = new_vec_seeds;
}

void ECALinterp::buildClusters0(Int_t iterations){
	m_Clusters_Container.clear();
	// figure out which cell belongs to each cluster
	for (auto seed_ID : m_vec_seeds){
		m_Clusters_Container[seed_ID] = SimpleCluster(seed_ID, m_Cells_Container, MyECal);
	}
	
	// find cells shared by several clusters
	std::vector<Long64_t>                     shared_cell_IDs;
	std::map<Long64_t, Double_t>              Eshared_container;
	std::map<Long64_t, std::vector<Long64_t>> seedIDs_container;
	for (auto cell_container : m_Cells_Container){ // <- loop over all cells
	 	if (cell_container.second.get_near_seed().size() > 1){
	  		shared_cell_IDs.push_back(cell_container.first);
			Eshared_container[cell_container.first] = cell_container.second.Energy();
			seedIDs_container[cell_container.first] = cell_container.second.get_near_seed();				
		}
	}
	// Energy redristibution for shared cells
	for (Int_t i = 0; i<iterations; ++i){
		for (auto shared_cell_ID : shared_cell_IDs){
			Int_t Nclusters = seedIDs_container[shared_cell_ID].size();
			std::vector<Double_t> Eeff_cluster;
			Double_t Eeff_clusters = 0.;
			for (Int_t j=0; j<Nclusters;++j){
				Long64_t cluster_id = (seedIDs_container[shared_cell_ID])[j];
				Eeff_cluster.push_back( m_Clusters_Container[cluster_id].get_energy()-m_Clusters_Container[cluster_id].m_cells_container[shared_cell_ID].Energy());
				Eeff_clusters += Eeff_cluster.back();
			}	
			for (Int_t j=0; j<Nclusters;++j){
				Double_t deltaE = Eshared_container[shared_cell_ID]*Eeff_cluster[j]/Eeff_clusters;
				Long64_t cluster_id = (seedIDs_container[shared_cell_ID])[j];
				m_Clusters_Container[cluster_id].m_cells_container[shared_cell_ID].set_Energy(deltaE);
				std::vector<Double_t>vec_energy={deltaE};
				m_Clusters_Container[cluster_id].m_cells_container[shared_cell_ID].set_vec_energy(vec_energy);
				std::map<Long64_t,SimpleCell> new_cell_container = m_Clusters_Container[cluster_id].get_cells_container();
				m_Clusters_Container[cluster_id].setupCluster(cluster_id, new_cell_container, MyECal);
			}
		}
	}
	std::cout << "eventNumber: "<< m_evtNumber << " ("<< m_vec_seeds.size() <<")\n";	
}

std::vector<Double_t> ECALinterp::seedFindingStats(ECALinterp &ecalTRUE, ECALinterp &ecalRECO, TString nfout, Int_t verbose){
	std::vector<Long64_t> vTRUE = ecalTRUE.getSeeds();
	std::vector<Long64_t> vRECO = ecalRECO.getSeeds();
	Int_t T1T2 =0;
	Int_t T1F2 =0;
	Int_t T2F1 =0;
	std::vector<TH2D> histoTRUE = ecalTRUE.monitor("seed");
	std::vector<TH2D> histoRECO = ecalRECO.monitor("seed");
	Int_t Nregions = histoTRUE.size();
	TH2D* diff[Nregions];
	TCanvas *c = new TCanvas("c_seedStat","c_seedStat", 1200, 900);
	for (Int_t i=Nregions-1;i>=0;--i){
		TString nh("");
		nh.Form("seedMap_region%d",i);
		diff[i] = (TH2D*)histoTRUE[i].Clone(nh);
		diff[i]->Reset();
		diff[i]->SetMaximum(1.0);
		diff[i]->SetMinimum(-1.0);
		Int_t NbinsX = diff[i]->GetXaxis()->GetNbins();
		Int_t NbinsY = diff[i]->GetYaxis()->GetNbins();
		for (Int_t k=1; k<NbinsX+1;++k){
			for (Int_t j=1; j<NbinsY+1;++j){
				Int_t seedTRUE = histoTRUE[i].GetBinContent(k,j);
				Int_t seedRECO = histoRECO[i].GetBinContent(k,j);
				Int_t out = 0;
				if ((seedTRUE != 0) && (seedRECO != 0)) {out = 0; ++T1T2;}
				else if ((seedTRUE != 0) && (seedRECO == 0)) {out = -1; ++T1F2;}
				else if ((seedTRUE == 0) && (seedRECO != 0)) {out = 1; ++T2F1;}
				else out = -2;
				diff[i]->SetBinContent(k,j, out);
			}
		}
		diff[i]->Draw("COLORZ same");
	}
	if (verbose>0){
		std::cout << "TRUE  POSITIVE : " << T1T2 << "   -->  " << (Double_t)T1T2/(Double_t)(T1T2+T1F2)*100 << "  % \n";
		std::cout << "TRUE  NEGATIVE : " << T1F2 << "   -->  " << (Double_t)T1F2/(Double_t)(T1T2+T1F2)*100 << "  % \n";
		std::cout << "FALSE POSITIVE : " << T2F1 << "   -->  " << (Double_t)T2F1/(Double_t)(T2F1+T1T2)*100 << "  % \n";
	}
	if (nfout!="none"){
		TFile fout(nfout+".root", "recreate");
		c->SaveAs(nfout+".pdf");
		fout.WriteTObject(c, c->GetName(), "overwrite");
		fout.Close();
	}
	delete c;
	std::vector<Double_t> res;
	res.push_back(T1T2);
	res.push_back(T1F2);
	res.push_back(T2F1);
	return res;
}



TObject* ECALinterp::clusterMonitor(TString nfout){
	TRandom3 Rand(0);
	std::vector<TH2D> histBase = this->monitor("energy", "L", "none");
	Int_t Nregions = histBase.size();
	TH2Poly h2p("contour", "contour",histBase[Nregions-1].GetXaxis()->GetXmin(), histBase[Nregions-1].GetXaxis()->GetXmax(),
																	 histBase[Nregions-1].GetYaxis()->GetXmin(), histBase[Nregions-1].GetYaxis()->GetXmax());
	h2p.SetLineWidth(4);
	h2p.SetLineColor(kRed);
	for (Int_t i = 0; i<Nregions; ++i) {
		TString nh("");
		nh.Form("ClusterMonitor_region%d", i);
		histBase[i].SetName(nh);
		histBase[i].SetTitle("Cluster Monitor");
	}
	for (auto cluster_container : m_Clusters_Container){
		std::vector<std::pair<Float_t,Float_t>> vertex_list = cluster_container.second.getBorder(MyECal);
		Int_t Nvertex = vertex_list.size();
		Double_t* x = new Double_t[Nvertex];
		Double_t* y = new Double_t[Nvertex];
		for (Int_t i = 0; i< Nvertex; ++i){
			x[i] = vertex_list[i].first;
			y[i] = vertex_list[i].second; 
		}
		h2p.AddBin(Nvertex,x,y);
		h2p.Fill(cluster_container.second.get_x(), cluster_container.second.get_y(), Rand.Rndm());
		delete [] x;
		delete [] y;
	}
	TCanvas c("c_"+nfout,"c_"+nfout,1200,900);
	for (Int_t i = Nregions-1; i>=0; --i){
		if(i==0)
			histBase[i].Draw("COLORZ same");
		else
			histBase[i].Draw("COL same");
	}
	h2p.Draw("L same");
	if (nfout != "none"){
		TFile fout(nfout+".root", "recreate");
		fout.WriteTObject(&c,c.GetName(),"overwrite");
		fout.WriteTObject(&h2p,h2p.GetName(),"overwrite");		
		fout.Close();
		c.SaveAs(nfout+".pdf");
	}
	return c.Clone("canv_"+nfout);
}

void ECALinterp::saveTreeParticles(TString nfout, TString ntout, Bool_t useCalib){
	TFile fout(nfout+".root", "RECREATE");
	TTree tout(ntout, ntout);
	Double_t x(0.), y(0.), z(0.), E(0.), px(0.), py(0.), pz(0.), t(0.);
	Int_t evtNumber(0), region(0), key(0);
	const Int_t kNparticles = 10;
	Int_t particles_keys[kNparticles];
	Long64_t seedID(0);
	tout.Branch("t",  &t,  "t/D");
	tout.Branch("x",  &x,  "x/D");
	tout.Branch("y",  &y,  "y/D");
	tout.Branch("z",  &z,  "z/D");
	tout.Branch("E",  &E,  "E/D");
	tout.Branch("px", &px, "px/D");
	tout.Branch("py", &py, "py/D");
	tout.Branch("pz", &pz, "pz/D");
	tout.Branch("key", &key, "key/I");
	tout.Branch("seedID", &seedID, "seedID/L");
	tout.Branch("eventNumber", &evtNumber, "eventNumber/I");
	tout.Branch("region", &region, "region/I");
	tout.Branch("particles_keys", particles_keys, "particles_keys[10]/L");
	for (auto cluster_container: m_Clusters_Container){
		SimpleCluster *cluster = &(cluster_container.second);
		if (useCalib){
		  x = cluster->get_calib_x();
		  y = cluster->get_calib_y();
		  E = cluster->get_calib_energy();
		} else { 
		  x = cluster->get_x();
		  y = cluster->get_y();
		  E = cluster->get_energy();
		}
		z = cluster->get_z();
		t = cluster->get_time();
		px = cluster->get_Px();
		py = cluster->get_Py();
		pz = cluster->get_Pz();
		key = cluster->get_key();
		seedID = cluster->get_seed();
		region = MyECal->get_RegionID(x,y);
		evtNumber = m_evtNumber;
		std::vector<Long64_t> vec_particles = cluster->get_vec_particles();
		Int_t Nparticles = vec_particles.size();
		for (Int_t i = 0; i<kNparticles; ++i){
			if (i>=Nparticles) particles_keys[i] = -1;
			else particles_keys[i] = vec_particles[i];
		}
		tout.Fill();
	}
	fout.WriteTObject(&tout, tout.GetName(),"overwrite");
	fout.Close();
}

void ECALinterp::reco(Double_t alpha, Double_t beta, Double_t Time_Smear, Double_t threshold, Bool_t mantainTRUEseeds){
	if (mantainTRUEseeds == false) m_vec_seeds.clear();
	this->Towers::reco(alpha, beta, Time_Smear, threshold, mantainTRUEseeds);
}

std::vector<Double_t> ECALinterp::getOccupancy(Double_t threshold){
	std::vector<Double_t>res;
	std::vector<Int_t> N_on;
	std::vector<Int_t> N_tot;
	std::vector<std::string> regions = this->MyECal->get_RegionNames();
	UInt_t Nregions = regions.size();
	for (UInt_t i = 0; i<Nregions+1;++i){
		res.push_back(0.0);
		N_on.push_back(0);
		N_tot.push_back(0);
	}
	
	std::vector<Double_t> xBorders = this->MyECal->get_xBorder();
	std::vector<Double_t> yBorders = this->MyECal->get_yBorder();
	std::vector<Double_t> xWbins   = this->MyECal->get_xBins();
	std::vector<Double_t> yWbins   = this->MyECal->get_yBins();
	if (xBorders.size() != Nregions+1 ||
		yBorders.size() != Nregions+1 ||
		xWbins.size()   != Nregions   ||
		yWbins.size()   != Nregions   ) {std::cout << "ERROR:  geometry is not well defined!!!!!" << std::endl; return res;};

	for (UInt_t i=0; i<Nregions; ++i){
		Double_t width = xBorders[i+1]*2;
		Double_t height= yBorders[i+1]*2;
		Int_t Ncol = (Int_t)  (width/xWbins[i]);
		Int_t Nraw = (Int_t) (height/yWbins[i]);
		if ( (xWbins[i]*Ncol-xBorders[i+1]*2)>1E-9 ) {std::cout << "ERROR:  geometry is not well defined!!!!!" << std::endl; return res;};
		if ( (yWbins[i]*Nraw-yBorders[i+1]*2)>1E-9 ) {std::cout << "ERROR:  geometry is not well defined!!!!!" << std::endl; return res;};
		Int_t Ncells = Ncol*Nraw;
		
		width = xBorders[i]*2;
		height= yBorders[i]*2;
		Ncol = (Int_t)  (width/xWbins[i]);
		Nraw = (Int_t)  (height/yWbins[i]);
		if ( (xWbins[i]*Ncol-xBorders[i]*2)>1E-9 ) {std::cout << "ERROR:  geometry is not well defined!!!!!" << std::endl; return res;};
		if ( (yWbins[i]*Nraw-yBorders[i]*2)>1E-9 ) {std::cout << "ERROR:  geometry is not well defined!!!!!" << std::endl; return res;};
		Ncells = Ncells - Ncol*Nraw;
		N_tot[0] += Ncells;
		N_tot[i+1]= Ncells;
	}

	for (auto container : m_Cells_Container){
		if (container.second.Energy()<threshold) continue;
		Int_t i_region = this->MyECal->get_RegionID(container.first);
		N_on[0] += 1;
		N_on[i_region] += 1;
	}
	for (UInt_t i=0; i<res.size();++i){
		res[i] = ( (Double_t)N_on[i] ) / ( (Double_t)N_tot[i]);
		//std::cout << N_on[i] << " / " << N_tot[i] << " = " << res[i] << "\n";
	}
	return res;
}


void ECALinterp::findSeeds1(Double_t E_T_th){
	// Cellular Autom Alg. + cand. merged pi0
	m_vec_seeds.clear();
	for (auto& cell_container : m_Cells_Container)
		cell_container.second.reset_near_seeds();
	
	Double_t E(0.0), E_T(0.0),E2(0.0), near_E(0.0); 
	Long64_t ID(0),ID2(0);
	Bool_t tmp_seed(true);
	for (auto& cell_container : m_Cells_Container){        // loop over all cell that got energy
		SimpleCell* cell = &(cell_container.second);
		if (cell->get_second_seed()) continue; //a second seed can not be a primary seed: continue
		//get info about the cell	
		E  = cell->Energy();
		E_T= cell->get_Et();			        // calculate transvers energy
		if (E_T<E_T_th) continue;                       // if E_T<E_th, this is not a seed: continue
		ID= cell_container.first;              // ID of the cell
		std::vector<Long64_t> vec_nearby_0;             // IDs of all cells in the neighbourhood
		std::vector<Long64_t> vec_nearby;  				// IDs of all cells in the neighbourhood, that got energy
		MyECal->get_nearby(ID, vec_nearby_0);           // fill vec_nearby_0
		tmp_seed = true;	                                // initialization of seed flag
		E2  = 0.0;
		ID2 = 0;                                        // ID for the second seed
		for (auto near_ID: vec_nearby_0){               // loop over the cells in the neighb.               
			auto it_near_cell = m_Cells_Container.find(near_ID); 
			if ( it_near_cell == m_Cells_Container.end()) continue; // if a neighb. cell didn't get energy: skip it
			vec_nearby.push_back(near_ID);              // fill vec_nearby
			near_E = it_near_cell->second.Energy();
			if (near_E > E){ // if energy of the neighb. cell is greater than the energy of this cell, 
				tmp_seed = false;                         // this is not a seed: switch off the seed flag and go to next cell
				break;
			}
			if (near_E > E2){
				E2 = near_E;
				ID2= near_ID;
			}
		}
		cell->set_seed(tmp_seed);
		if (tmp_seed){                                   // if a seed is found
			cell->set_near(vec_nearby);                  // tell the seed cell, what is its neigb. 
			cell->Add_near_seed(ID);     
			m_vec_seeds.push_back(ID);                   // add the seed cell to the list of all seed in the event
			for (auto cell_near_ID : vec_nearby)         // loop over all the cells in the neighb. to
				m_Cells_Container.at(cell_near_ID).Add_near_seed(ID); //tell them, they have a seed
			//look for a second seed
			continue;
			// get info about cell2
			SimpleCell * cell2 = &(m_Cells_Container.find(ID2)->second);
			Double_t E_T2=cell2->get_Et();
			if (E_T2<E_T_th) continue; //if Etrans is too low, cell2 is not a second seed: continue
			std::vector<Long64_t> vec_nearby2_0;    //vector for the neighb. of the second seed
			std::vector<Long64_t> vec_nearby2;      //vector for the neighb. of the second seed, tat got energy
			MyECal->get_nearby(ID2, vec_nearby2_0); // fill vec_nearby2_0
			Bool_t tmp_seed2 = true;
			for (auto near_ID: vec_nearby2_0){
				auto it_near_cell2 = m_Cells_Container.find(near_ID); 
				if (it_near_cell2 == m_Cells_Container.end()) continue;
				vec_nearby2.push_back(near_ID);
				if (it_near_cell2->second.Energy() > E2 && near_ID != ID){
					tmp_seed2 = false;
					break;
				}
			}
			cell2->set_second_seed(tmp_seed2);
			cell2->set_seed(tmp_seed2);                  //<-- should be a temporary choice
			if (tmp_seed2){                              // if a second seed is found
				cell2->set_near(vec_nearby2);            // tell the seed cell, what is its neigb. 
				cell2->Add_near_seed(ID2);  
				for (auto cell_near_ID : vec_nearby2)    // loop over all the cells in the neighb. to
					m_Cells_Container.at(cell_near_ID).Add_near_seed(ID2); //tell them, they have a seed   
				m_vec_seeds.push_back(ID2);	
				cell->set_merged_link(ID2);
				cell2->set_merged_link(ID);
			}	
		}
	}
	std::sort(m_vec_seeds.begin(), m_vec_seeds.end());
	auto it = std::unique(m_vec_seeds.begin(), m_vec_seeds.end());
	m_vec_seeds.resize(std::distance(m_vec_seeds.begin(),it));
}




void ECALinterp::findSeeds2(Double_t E_T_th){ //CorrectSecondSeed
	// Cellular Autom Alg. + cand. merged pi0
	m_vec_seeds.clear();
	for (auto& cell_container : m_Cells_Container)
		cell_container.second.reset_near_seeds();
	Double_t E(0.0), E_T(0.0),E2(0.0), near_E(0.0); 
	Long64_t ID(0),ID2(0);
	Bool_t tmp_seed(true);
	for (auto& cell_container : m_Cells_Container){        // loop over all cell that got energy
		SimpleCell* cell = &(cell_container.second);
		if (cell->get_second_seed()) continue; //a second seed can not be a primary seed: continue
		//get info about the cell	
		E  = cell->Energy();
		E_T= cell->get_Et();			        // calculate transvers energy
		if (E_T<E_T_th) continue;               // if E_T<E_th, this is not a seed: continue
		ID= cell_container.first;               // ID of the cell
		std::vector<Long64_t> vec_nearby_0;             // IDs of all cells in the neighbourhood
		std::vector<Long64_t> vec_nearby;  				// IDs of all cells in the neighbourhood, that got energy
		MyECal->get_nearby(ID, vec_nearby_0);           // fill vec_nearby_0
		tmp_seed = true;	                            // initialization of seed flag
		E2  = 0.0;
		ID2 = 0;                                        // ID for the second seed
		for (auto near_ID: vec_nearby_0){               // loop over the cells in the neighb.               
			auto it_near_cell = m_Cells_Container.find(near_ID); 
			if ( it_near_cell == m_Cells_Container.end()) continue; // if a neighb. cell didn't get energy: skip it
			vec_nearby.push_back(near_ID);              // fill vec_nearby
			near_E = it_near_cell->second.Energy();
			if (near_E > E){ // if energy of the neighb. cell is greater than the energy of this cell, 
				tmp_seed = false;                         // this is not a seed: switch off the seed flag and go to next cell
				break;
			}
			if (near_E > E2){
				E2 = near_E;
				ID2= near_ID;
			}
		}
		cell->set_seed(tmp_seed);
		if (tmp_seed){                                   // if a seed is found
			cell->set_near(vec_nearby);                  // tell the seed cell, what is its neigb. 
			cell->Add_near_seed(ID);     
			m_vec_seeds.push_back(ID);                   // add the seed cell to the list of all seed in the event
			for (auto cell_near_ID : vec_nearby)         // loop over all the cells in the neighb. to
				m_Cells_Container.at(cell_near_ID).Add_near_seed(ID); //tell them, they have a seed
			//look for a second seed
			// get info about cell2
			SimpleCell * cell2 = &(m_Cells_Container.find(ID2)->second);
			Double_t E_T2=cell2->get_Et();
			if (E_T2<E_T_th) continue; //if Etrans is too low, cell2 is not a second seed: continue
			if (cell2->get_seed_key().size() == 0) continue;
			std::vector<Long64_t> vec_nearby2_0;    //vector for the neighb. of the second seed
			std::vector<Long64_t> vec_nearby2;      //vector for the neighb. of the second seed, that got energy
			MyECal->get_nearby(ID2, vec_nearby2_0); // fill vec_nearby2_0
			Bool_t tmp_seed2 = true;
			for (auto near_ID: vec_nearby2_0){
				auto it_near_cell2 = m_Cells_Container.find(near_ID); 
				if (it_near_cell2 == m_Cells_Container.end()) continue;
				vec_nearby2.push_back(near_ID);
				//if (it_near_cell2->second.Energy() > E2 && near_ID != ID){
				//	tmp_seed2 = false;
				//	break;
				//}
			}
			cell2->set_second_seed(tmp_seed2);
			cell2->set_seed(tmp_seed2);                  //<-- should be a temporary choice
			if (tmp_seed2){                              // if a second seed is found
				cell2->set_near(vec_nearby2);            // tell the seed cell, what is its neigb. 
				cell2->Add_near_seed(ID2);  
				for (auto cell_near_ID : vec_nearby2)    // loop over all the cells in the neighb. to
					m_Cells_Container.at(cell_near_ID).Add_near_seed(ID2); //tell them, they have a seed   
				m_vec_seeds.push_back(ID2);	
				cell->set_merged_link(ID2);
				cell2->set_merged_link(ID);
			}
		}
	}
	UInt_t Nseeds_old = m_vec_seeds.size();
	std::sort(m_vec_seeds.begin(), m_vec_seeds.end());
	auto it = std::unique(m_vec_seeds.begin(), m_vec_seeds.end());
	m_vec_seeds.resize(std::distance(m_vec_seeds.begin(),it));
	if (Nseeds_old != m_vec_seeds.size())
	std::cout << "proca puttana c'era due volte lo stesso seed!!!\n";
}

void ECALinterp::findSeeds3(Double_t E_T_th){
	// Cellular Autom Alg. + cand. merged pi0
	m_vec_seeds.clear();
	for (auto& cell_container : m_Cells_Container)
		cell_container.second.reset_near_seeds();
	Double_t E(0.0), E_T(0.0),E2(0.0), near_E(0.0); 
	Long64_t ID(0),ID2(0);
	Bool_t tmp_seed(true);
	for (auto& cell_container : m_Cells_Container){        // loop over all cell that got energy
		SimpleCell* cell = &(cell_container.second);
		if (cell->get_second_seed()) continue; //a second seed can not be a primary seed: continue
		//get info about the cell	
		E  = cell->Energy();
		E_T= cell->get_Et();			        // calculate transvers energy
		if (E_T<E_T_th) continue;               // if E_T<E_th, this is not a seed: continue
		ID= cell_container.first;               // ID of the cell
		std::vector<Long64_t> vec_nearby_0;             // IDs of all cells in the neighbourhood
		std::vector<Long64_t> vec_nearby;  				// IDs of all cells in the neighbourhood, that got energy
		MyECal->get_nearby(ID, vec_nearby_0);           // fill vec_nearby_0
		tmp_seed = true;	                            // initialization of seed flag
		E2  = 0.0;
		ID2 = 0;                                        // ID for the second seed
		for (auto near_ID: vec_nearby_0){               // loop over the cells in the neighb.               
			auto it_near_cell = m_Cells_Container.find(near_ID); 
			if ( it_near_cell == m_Cells_Container.end()) continue; // if a neighb. cell didn't get energy: skip it
			vec_nearby.push_back(near_ID);              // fill vec_nearby
			near_E = it_near_cell->second.Energy();
			if (near_E > E){ // if energy of the neighb. cell is greater than the energy of this cell, 
				tmp_seed = false;                         // this is not a seed: switch off the seed flag and go to next cell
				break;
			}
			if (near_E > E2){
				E2 = near_E;
				ID2= near_ID;
			}
		}
		cell->set_seed(tmp_seed);
		if (tmp_seed){                                   // if a seed is found
			cell->set_near(vec_nearby);                  // tell the seed cell, what is its neigb. 
			cell->Add_near_seed(ID);     
			m_vec_seeds.push_back(ID);                   // add the seed cell to the list of all seed in the event
			for (auto cell_near_ID : vec_nearby)         // loop over all the cells in the neighb. to
				m_Cells_Container.at(cell_near_ID).Add_near_seed(ID); //tell them, they have a seed
			//look for a second seed
			// get info about cell2
			SimpleCell * cell2 = &(m_Cells_Container.find(ID2)->second);
			Double_t E_T2=cell2->get_Et();
			if (E_T2<60) continue; //if Etrans is too low, cell2 is not a second seed: continue
			if (TMath::Log(TMath::Abs(cell->Time()-cell2->Time()))<-1.5) continue;
			std::vector<Long64_t> vec_nearby2_0;    //vector for the neighb. of the second seed
			std::vector<Long64_t> vec_nearby2;      //vector for the neighb. of the second seed, that got energy
			MyECal->get_nearby(ID2, vec_nearby2_0); // fill vec_nearby2_0
			Bool_t tmp_seed2 = true;
			for (auto near_ID: vec_nearby2_0){
				auto it_near_cell2 = m_Cells_Container.find(near_ID); 
				if (it_near_cell2 == m_Cells_Container.end()) continue;
				vec_nearby2.push_back(near_ID);
				//if (it_near_cell2->second.Energy() > E2 && near_ID != ID){
				//	tmp_seed2 = false;
				//	break;
				//}
			}
			cell2->set_second_seed(tmp_seed2);
			cell2->set_seed(tmp_seed2);                  //<-- should be a temporary choice
			if (tmp_seed2){                              // if a second seed is found
				cell2->set_near(vec_nearby2);            // tell the seed cell, what is its neigb. 
				cell2->Add_near_seed(ID2);  
				for (auto cell_near_ID : vec_nearby2)    // loop over all the cells in the neighb. to
					m_Cells_Container.at(cell_near_ID).Add_near_seed(ID2); //tell them, they have a seed   
				m_vec_seeds.push_back(ID2);	
				cell->set_merged_link(ID2);
				cell2->set_merged_link(ID);
			}
		}
	}
	std::sort(m_vec_seeds.begin(), m_vec_seeds.end());
	auto it = std::unique(m_vec_seeds.begin(), m_vec_seeds.end());
	m_vec_seeds.resize(std::distance(m_vec_seeds.begin(),it));
}









void ECALinterp::flagSecondSeeds(Double_t E_T_th){
	for (auto& seed_ID: m_vec_seeds){
		SimpleCell* seed_cell = &(m_Cells_Container.at(seed_ID));
		seed_cell->set_second_seed(false);
		std::vector <Long64_t> vec_nearby;
		MyECal->get_nearby(seed_ID,vec_nearby);
		for (auto& near_ID : vec_nearby){
			auto near_seed_ID = std::find(m_vec_seeds.begin(), m_vec_seeds.end(), near_ID);
			if (near_seed_ID == m_vec_seeds.end()) continue;
			SimpleCell* near_seed_cell = &(m_Cells_Container.at(*near_seed_ID));
			if (seed_cell->Energy() < near_seed_cell->Energy()){
				seed_cell->set_second_seed(true);
				break;		
			}
		}
	}
}

inline void ECALinterp::energy_redistribution(Int_t iterations){
	// find cells shared by several clusters
	std::vector<Long64_t>                     shared_cell_IDs;
	std::map<Long64_t, Double_t>              Eshared_container;
	std::map<Long64_t, std::vector<Long64_t>> seedIDs_container;
	for (auto& cell_container : m_Cells_Container){ // <- loop over all cells
	 	auto cellID = cell_container.first;
	 	auto cell = &(cell_container.second);
	 	Double_t t = cell->get_time();
	 	if (cell_container.second.get_near_seed().size() > 1){
	  		shared_cell_IDs.push_back(cellID);
			Eshared_container[cellID] = cell->Energy();
			seedIDs_container[cellID] = cell->get_near_seed();	
			Double_t t = cell->get_time();			
		}
	}
	// Energy redristibution for shared cells
	for (Int_t i = 0; i<iterations; ++i){
		//for (auto shared_cell_ID : shared_cell_IDs){
		for (auto shared_cell_ID = shared_cell_IDs.begin(); shared_cell_ID != shared_cell_IDs.end(); ++shared_cell_ID){
			auto seedIDs_container_at_shared_cell_ID = &(seedIDs_container.at(*shared_cell_ID));
			Int_t Nclusters = seedIDs_container_at_shared_cell_ID->size();
			std::vector<Double_t> Eeff_cluster;
			Double_t Eeff_clusters = 0.;
			for (Int_t j=0; j<Nclusters;++j){
				Long64_t cluster_id = (*seedIDs_container_at_shared_cell_ID)[j];
				auto m_Clusters_Container_at_cluster_id = &(m_Clusters_Container.at(cluster_id));
				Eeff_cluster.push_back( m_Clusters_Container_at_cluster_id->get_energy()-m_Clusters_Container_at_cluster_id->m_cells_container.at(*shared_cell_ID).Energy());
				Eeff_clusters += Eeff_cluster.back();
			}	
			for (Int_t j=0; j< Nclusters ;++j){
				Double_t deltaE = Eshared_container.at(*shared_cell_ID)*Eeff_cluster[j]/Eeff_clusters;
				Long64_t cluster_id = (*seedIDs_container_at_shared_cell_ID).at(j);
				auto m_Clusters_Container_at_cluster_id = &(m_Clusters_Container.at(cluster_id));
				auto m_Clusters_Container_at_cluster_id_m_cells_container_at_shared_cell_ID = &(m_Clusters_Container_at_cluster_id->m_cells_container.at(*shared_cell_ID));
				m_Clusters_Container_at_cluster_id_m_cells_container_at_shared_cell_ID->set_Energy(deltaE);
				std::vector<Double_t>vec_energy={deltaE};
				m_Clusters_Container_at_cluster_id_m_cells_container_at_shared_cell_ID->set_vec_energy(vec_energy);
				std::map<Long64_t,SimpleCell> new_cell_container = m_Clusters_Container_at_cluster_id->get_cells_container();
				m_Clusters_Container_at_cluster_id->setupCluster(cluster_id, new_cell_container, MyECal);
			}
		}
	}
}

Int_t ECALinterp::switchOff_fakeMergedPi0(){
	Int_t DeltaMergedPi0 = 0;
	std::vector<Long64_t> m_vec_seeds_new;
	std::cout << "("<<m_vec_seeds.size()<<")";
	for (auto ID2 : m_vec_seeds){
		SimpleCell* cell_2seed = &(m_Cells_Container.at(ID2)); 
		if (!cell_2seed->get_second_seed()){ //primary seeds
			m_vec_seeds_new.push_back(ID2);
			continue;
		} 

		Long64_t ID1 = cell_2seed->get_merged_link();
		SimpleCluster *cl1 = &(m_Clusters_Container.at(ID1));
		SimpleCluster *cl2 = &(m_Clusters_Container.at(ID2));
		
		// ****** requirements for merged pi0
		// neutral -> to add
		Bool_t isNeutral = (cl1->get_mass()<1E-4);
		if (isNeutral || 1){
			// gamma-gamma distance cut
			SimplePi0 pi0(cl1, cl2);
			Double_t cell_size = MyECal->get_xBins()[pi0.m_region-1]; //assuming square shape for cells
			if (pi0.m_d_gammas_min < cut_mergedPi0_d_gammas_min*cell_size){	
				if (pi0.m_M>cut_mergedPi0_M_min && pi0.m_M<cut_mergedPi0_M_max){
					m_vec_seeds_new.push_back(ID2);
					//if the following "continue" is commented, all second seed are switched off!!!!
					continue; 
				}
			}	
		}
		//if here, the merged pi0 hypo. is false: switch off
		++DeltaMergedPi0;
		cell_2seed->set_seed(false);
		cell_2seed->set_second_seed(false);
		cell_2seed->set_merged_link(-1);
		m_Cells_Container.at(ID1).set_merged_link(-1);
		for (auto& near_cell2_ID : cell_2seed->get_near()){
			m_Cells_Container.at(near_cell2_ID).remove_near_seed(ID2);
		}
		std::vector<Long64_t> empty_neighb;
		cell_2seed->set_near(empty_neighb);
		cell_2seed->remove_near_seed(ID2);
		//cell_2seed->reset_near_seeds();
		//cell_2seed->Add_near_seed(ID1);
	}
	m_vec_seeds = m_vec_seeds_new;
	return DeltaMergedPi0;
}

void ECALinterp::buildClusters1(Int_t iterations){
	m_Clusters_Container.clear();
	// figure out which cell belongs to each cluster
	for (auto seed_ID : m_vec_seeds){
		m_Clusters_Container[seed_ID] = SimpleCluster(seed_ID, m_Cells_Container, MyECal);
	}
	energy_redistribution(iterations);
	Int_t DeltaMergedPi0 = 0;
	std::cout << "eventNumber: "<< m_evtNumber << " deltaMergedPi0: ";
	while (DeltaMergedPi0 != 0){
		DeltaMergedPi0 = switchOff_fakeMergedPi0();
		std::cout << "  " << DeltaMergedPi0;
		if (DeltaMergedPi0 == 0) break;
		m_Clusters_Container.clear();
		for (auto seed_ID : m_vec_seeds){
			m_Clusters_Container[seed_ID] = SimpleCluster(seed_ID, m_Cells_Container, MyECal);
		}
		energy_redistribution(iterations);
	}
	std::cout << std::endl;
}


void ECALinterp::fillContainer_Pi0merged(){
	std::map<Long64_t,SimpleCluster>::iterator it = m_Clusters_Container.begin();
	while(it != m_Clusters_Container.end()){
		if (!m_Cells_Container.at(it->first).get_second_seed()) {++it; continue;}
		SimplePi0 mergedPi0( &( m_Clusters_Container.at(it->second.get_merged_link()) ), &(it->second)  );
		mergedPi0.m_isMerged = true;
		mergedPi0.m_isResolv = false;
		m_MergedPi0_Container[it->second.get_merged_link()][it->first] = mergedPi0;
		++it;
	}
	std::cout << " event Number:  " << m_evtNumber << " #pi merged:  "<< m_MergedPi0_Container.size() <<std::endl;
}
void ECALinterp::fillContainer_Pi0resolv(){
	std::map<Long64_t,SimpleCluster>::iterator it1 = m_Clusters_Container.begin();
	while(it1 != m_Clusters_Container.end()){
		if (it1->second.get_merged_link() != -1) {++it1; continue;}
		Double_t px1 = it1->second.get_Px();
		Double_t py1 = it1->second.get_Py();
		if ( (px1*px1 + py1*py1) < 200*200 ) {++it1; continue;} // pt check
		std::map<Long64_t,SimpleCluster>::iterator it2 = it1;
		++it2;
		while(it2 != m_Clusters_Container.end()){
			if (it2->second.get_merged_link() != -1) {++it2; continue;}
			Double_t px2 = it2->second.get_Px();
			Double_t py2 = it2->second.get_Py();
			if ( (px2*px2 + py2*py2) < 200*200 ) {++it2; continue;} // pt check
			SimplePi0 resolvPi0(&(it1->second), &(it2->second));
			if (resolvPi0.m_M > 230) {++it2; continue;}
			if (resolvPi0.m_M <  40) {++it2; continue;}
			resolvPi0.m_isMerged = false;
			resolvPi0.m_isResolv = true;
			m_ResolvPi0_Container[it1->first][it2->first] = resolvPi0;
			++it2;
		}
		++it1;
	}
	std::cout << " event Number:  " << m_evtNumber << " #pi resolved:  "<< m_ResolvPi0_Container.size() <<std::endl;
}
std::list<std::pair<Long64_t,Long64_t>> ECALinterp::findOverlappingClusters(){
	std::list<std::pair<Long64_t,Long64_t>> overlapping_cluster_IDs;
	Long64_t min(0), max(0);
	for (auto& cluster_container : m_Clusters_Container){ // <- loop over all cluster 
	 	auto clusterID      =   cluster_container.first  ;
	 	auto cluster        = &(cluster_container.second);
	 	auto cell_container = &(cluster->m_cells_container);
	 	for (auto it_cell = cell_container->begin(); it_cell != cell_container->end(); ++it_cell){ // loop over cell in cluster
	 		auto near_seeds = it_cell->second.get_near_seed();
	 		if (near_seeds.size() < 2) continue;
	 		for (const auto& seed : near_seeds){
	 			if (seed == clusterID) continue;
	 			if (seed > clusterID){
	 				min = clusterID;
	 				max = seed;
	 			} else {
	 				min = seed;
	 				max = clusterID;
	 			}
	 			overlapping_cluster_IDs.emplace_back(min, max);
	 		}
		} // end loop on cells in cluster
	} // end loop on clusters
	overlapping_cluster_IDs.sort();
	overlapping_cluster_IDs.unique();
	return overlapping_cluster_IDs;
}
void ECALinterp::buildClusters2(Int_t iterations){
	m_Clusters_Container.clear();
	// figure out which cell belongs to each cluster
	std::sort(m_vec_seeds.begin(), m_vec_seeds.end());
	for (auto& seed_ID : m_vec_seeds){
			m_Clusters_Container.insert(
				m_Clusters_Container.end(), 
				std::make_pair(seed_ID, SimpleCluster(seed_ID, m_Cells_Container, MyECal))
				);

	}
	// find clusters pairs with shared cells
	std::list<std::pair<Long64_t,Long64_t>> overlapping_cluster_IDs=findOverlappingClusters();     // container of cluster pairs with shared cells IDs
	// energy redistribution for shared cells
	for (auto& pair : overlapping_cluster_IDs){
		energy_redistribution_pro(pair.first, pair.second, iterations);
	}
	Int_t DeltaMergedPi0 = -1;
	std::cout << "eventNumber: "<< m_evtNumber << " deltaMergedPi0: ";
	while (DeltaMergedPi0 != 0){
		DeltaMergedPi0 = switchOff_fakeMergedPi0();
		std::cout << "  " << DeltaMergedPi0;
		if (DeltaMergedPi0 == 0) break;
		m_Clusters_Container.clear();
		overlapping_cluster_IDs.clear();
		for (auto seed_ID : m_vec_seeds){
			//m_Clusters_Container[seed_ID] = SimpleCluster(seed_ID, m_Cells_Container, MyECal);
			m_Clusters_Container.insert(
				m_Clusters_Container.end(),
				std::make_pair(seed_ID, SimpleCluster(seed_ID, m_Cells_Container, MyECal))
				);
		}
		overlapping_cluster_IDs=findOverlappingClusters();
		for (auto& pair : overlapping_cluster_IDs){
			energy_redistribution_pro(pair.first, pair.second, iterations);
		}	
	}
	Long64_t clusterID = 0;
	SimpleCluster *cluster = nullptr;
	Int_t Noverlaps = 0;
	Double_t dist_min = 99999.9;
	for (auto it = m_Clusters_Container.begin(); it != m_Clusters_Container.end(); ++it){
		clusterID = it->first;
		cluster = &(it->second);
		cluster->finalizeCluster(m_Cells_Container, MyECal, m_isSimpleTest);
		Noverlaps = 0;
		//dist_min = 99999.9;
		for (auto& pair : overlapping_cluster_IDs){
			if (pair.first == clusterID){
				++Noverlaps;
			} else if (pair.second == clusterID){
				++Noverlaps;
			}
		}
		cluster->set_Noverlaps(Noverlaps);
	}

	//std::cout << std::endl;
}
Double_t ECALinterp::func(Double_t *var, Double_t *par){
  Double_t x = var[0];
  Double_t y = var[1];
  Double_t f = 0.1032578;//probability of core/tail
  Double_t s = 1.57363;//
  Double_t s2 = s*s;
  Double_t R = 0.13315;
  Double_t r = sqrt(x*x+y*y);
  Double_t value = f/s2*exp(-(r*r)/s2/2) + (1-f)*2*pow(R,2)*pow(r*r+R*R,-2);
  return value/2.0/TMath::Pi();
}
Double_t ECALinterp::cell_integral(Double_t x1, Double_t x2, Double_t y1, Double_t y2, Double_t moliere){
  double x1_m = x1/moliere;
  double x2_m = x2/moliere;
  double y1_m = y1/moliere;
  double y2_m = y2/moliere;
  TF2* f = new TF2("f",func,x1_m,x2_m,y1_m,y2_m);
  return f->Integral(x1_m,x2_m,y1_m,y2_m);
}



void ECALinterp::energy_redistribution_pro(Long64_t &ID_I, Long64_t &ID_J, Int_t &iterations){
	Bool_t verbose = false;
	auto cl_I = &(m_Clusters_Container.find(ID_I)->second);
	auto cl_J = &(m_Clusters_Container.find(ID_J)->second);
	auto cells_I = cl_I->get_cells_container();
	auto cells_J = cl_J->get_cells_container();
	//auto vec_cells_I = cl_I->get_cells();
	auto vec_cells_J = cl_J->get_cells();
	Double_t E_seed_I = cells_I.at(ID_I).Energy();
	Double_t E_seed_J = cells_J.at(ID_J).Energy();
	Double_t E_I = cl_I->get_energy() - E_seed_J;
	Double_t E_J = cl_J->get_energy() - E_seed_I;
	Double_t frac_shared_I = E_I / (E_I+E_J);
	Double_t frac_shared_J = E_J / (E_I+E_J);
	std::list<Long64_t> shared_cells_IDs; 
	std::map<Long64_t, Double_t> E_meas_I;
	std::map<Long64_t, Double_t> E_meas_J;
	Double_t E_i, E_j, E_i_0, E_j_0;
	Long64_t cell_ID;
	SimpleCell* cell_i=nullptr; 
	SimpleCell* cell_j=nullptr;
	//start of step 0
	for (auto& cell_container : cells_I){
		cell_i = &(cell_container.second);
		cell_ID = cell_i->get_CellID(); 
		if (std::find(vec_cells_J.begin(), vec_cells_J.end(), cell_ID) != vec_cells_J.end()){
			shared_cells_IDs.push_back(cell_ID);
			E_i = cell_i->Energy();
			E_meas_I[cell_ID] = E_i;
			if 		(cell_ID == ID_I) continue;
			else if (cell_ID == ID_J) cell_i->set_Energy( 0 );
			else cell_i->set_Energy( frac_shared_I*E_i );
		} else continue;
	}
	for (auto& cell_container : cells_J){
		auto cell_j = &(cell_container.second);
		cell_ID = cell_j->get_CellID(); 
		if (std::find(shared_cells_IDs.begin(), shared_cells_IDs.end(), cell_ID) != shared_cells_IDs.end()){
			E_j = cell_j->Energy();
			E_meas_J[cell_ID] = E_j;
			if 		(cell_ID == ID_J) continue;
			else if (cell_ID == ID_I) cell_j->set_Energy( 0 );
			else cell_j->set_Energy( frac_shared_J*E_j );
		} else continue;	
	}
	cl_I->setupCluster(ID_I, cells_I, MyECal);
	cl_J->setupCluster(ID_J, cells_J, MyECal);
	//end of step 0

	std::map<Long64_t, std::pair< std::pair<SimpleCell*,Double_t>, std::pair<SimpleCell*, Double_t> > > shared_cells_ref;
	for (auto cell_ID : shared_cells_IDs){
		std::pair<SimpleCell*,Double_t>  p_i (&(cells_I.at(cell_ID)), E_meas_I.at(cell_ID));
		std::pair<SimpleCell*,Double_t>  p_j (&(cells_J.at(cell_ID)), E_meas_J.at(cell_ID));
		shared_cells_ref[cell_ID] = std::make_pair(p_i, p_j);
	}

	Double_t X_I(0.), Y_I(0.), X_J(0.), Y_J(0.), Rmoliere(0.);
	for (Int_t iter = 0; iter < iterations; ++iter){
		X_I = cl_I->get_calib_x();
	  	Y_I = cl_I->get_calib_y();
		X_J = cl_J->get_calib_x();
	  	Y_J = cl_J->get_calib_y();
	  	for (auto& p_shared_cells : shared_cells_ref){
	    	cell_ID = p_shared_cells.first;
	    	Rmoliere = MyECal->get_moliere_cellID(cell_ID);
	    	const auto& cells_E0 = &(p_shared_cells.second);
	    	cell_i  = cells_E0->first.first;
	    	E_i_0   = cells_E0->first.second;
	    	cell_j  = cells_E0->second.first;
	    	E_j_0   = cells_E0->second.second;
	    	E_i = E_I * cell_integral(cell_i->x1() - X_I,
					    			  cell_i->x2() - X_I,
									  cell_i->y1() - Y_I,
									  cell_i->y2() - Y_I, Rmoliere); 
	    	E_j = E_J * cell_integral(cell_j->x1() - X_J,
									  cell_j->x2() - X_J,
									  cell_j->y1() - Y_J,
									  cell_j->y2() - Y_J, Rmoliere); 
	    	cell_i->set_Energy( E_i_0 * E_i / (E_i + E_j));
	    	cell_j->set_Energy( E_j_0 * E_j / (E_i + E_j));
	  	}
	  	cl_I->setupCluster(ID_I, cells_I, MyECal);
	  	cl_J->setupCluster(ID_J, cells_J, MyECal);
	}

}




#endif
