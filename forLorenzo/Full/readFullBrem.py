import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-i','--input', type = str, dest = 'input', default = 'ciccio')
args = parser.parse_args()

from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *
#from DecayTreeTuple import Configuration

# Configure DaVinci
#DaVinci().TupleFile = "tuple.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = "Upgrade"
DaVinci().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]
DaVinci().PrintFreq = 1

DaVinci().DDDBtag = "dddb-20171126"
DaVinci().CondDBtag = "sim-20171126-vc-md100"

from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20171126"
LHCbApp().CondDBtag = "sim-20171126-vc-md100"
LHCbApp().DataType = "Upgrade"
LHCbApp().Detectors = ['VP', 'UT', 'FT', 'Rich1Pmt', 'Rich2Pmt', 'Ecal', 'Hcal', 'Muon', 'Magnet', 'Tr' ]
LHCbApp().Simulation = True
"""
from Configurables import CondDB
CondDB().Upgrade = True
CondDB().Tags['DDDB'] = 'ecal-occupancy-simulation'


from Configurables import LHCbApp

LHCbApp().DDDBtag = 'upgrade/ecal-occupancy-simulation'
LHCbApp().CondDBtag ='upgrade/sim-20171127-vc-md100'
LHCbApp().Simulation = True
"""
import GaudiPython

from GaudiPython.Bindings import gbl

MCParticle = gbl.LHCb.MCParticle
State = gbl.LHCb.State
MCVertex = gbl.LHCb.MCVertex
XYZPoint = gbl.ROOT.Math.XYZPoint

appMgr = GaudiPython.AppMgr()
evtsvc = appMgr.evtsvc()
evtsel = appMgr.evtsel()
toolsvc = appMgr.toolsvc()

#evtsel.open('out/0/Standalone/Gauss-11102404-20ev-20191209.xgen')
#evtsel.open('out/0/Full/Gauss-11102404-20ev-20191209.sim')
evtsel.open('%s'%(args.input))


from math import *
import ROOT

from ROOT import TChain, TTree, TRandom3, TFile

myRandom = TRandom3(0)
clight = 299.792458

from array import array
runNumber = array('L',[0])
eventNumber = array('L',[0])
X_ID = array('i',[0])
X_KEY = array('i',[0])
X_MID = array('i',[0])
X_MKEY = array('i',[0])
X_GMID = array('i',[0])
X_GMKEY = array('i',[0])
X_GGMID = array('i',[0])
X_GGMKEY = array('i',[0])

X_PX = array('d',[0])
X_PY = array('d',[0])
X_PZ = array('d',[0])
X_M = array('d',[0])

NMAX = 200
X_TRACKX = array('d',NMAX*[0.])
X_TRACKY = array('d',NMAX*[0.])
X_TRACKZ = array('d',NMAX*[0.])
X_TRACKT = array('d',NMAX*[0.])
X_TRACKPX = array('d',NMAX*[0.])
X_TRACKPY = array('d',NMAX*[0.])
X_TRACKPZ = array('d',NMAX*[0.])
X_TRACKL = array('d',[0])
X_TRACKN = array('i',[0])

X_OVX = array('d',[0])
X_OVY = array('d',[0])
X_OVZ = array('d',[0])
X_OVT = array('d',[0])
X_OVK = array('i',[0])
X_OVTYP = array('i',[0])
X_OVPROD = array('i',[0])

X_PVX = array('d',[0])
X_PVY = array('d',[0])
X_PVZ = array('d',[0])
X_PVT = array('d',[0])
X_PVK = array('i',[0])

X_ENDVX = array('d',[0])
X_ENDVY = array('d',[0])
X_ENDVZ = array('d',[0])
X_ENDVT = array('d',[0])
X_ENDVK = array('i',[0])
X_ENDVTYP = array('i',[0])
X_ENDVPROD = array('i',[0])

X_CALOVX = array('d',[0])
X_CALOVY = array('d',[0])
X_CALOVZ = array('d',[0])
X_CALOVT = array('d',[0])

X_CALOPX = array('d',[0])
X_CALOPY = array('d',[0])
X_CALOPZ = array('d',[0])

tupleFile = TFile.Open('tupleFullBrem_Z.root','RECREATE')
ntuple = TTree("ntp","ntp")
ntuple.Branch('runNumber',    runNumber,    'runNumber/L')
ntuple.Branch('eventNumber',  eventNumber,  'eventNumber/L')

ntuple.Branch('X_ID',    X_ID,    'X_ID/I')
ntuple.Branch('X_KEY',   X_KEY,   'X_KEY/I')
ntuple.Branch('X_MID',   X_MID,   'X_MID/I')
ntuple.Branch('X_MKEY',  X_MKEY,  'X_MKEY/I')
ntuple.Branch('X_GMID',  X_GMID,  'X_GMID/I')
ntuple.Branch('X_GMKEY', X_GMKEY, 'X_GMKEY/I')
ntuple.Branch('X_GGMID', X_GGMID, 'X_GGMID/I')
ntuple.Branch('X_GGMKEY',X_GGMKEY,'X_GGMKEY/I')
ntuple.Branch('X_PX',X_PX,'X_PX/D')
ntuple.Branch('X_PY',X_PY,'X_PY/D')
ntuple.Branch('X_PZ',X_PZ,'X_PZ/D')
ntuple.Branch('X_M', X_M, 'X_M/D')
ntuple.Branch('X_OVX',X_OVX,'X_OVX/D')
ntuple.Branch('X_OVY',X_OVY,'X_OVY/D')
ntuple.Branch('X_OVZ',X_OVZ,'X_OVZ/D')
ntuple.Branch('X_OVT',X_OVT,'X_OVT/D')
ntuple.Branch('X_OVK',X_OVK,'X_OVK/I')
ntuple.Branch('X_OVTYP',X_OVTYP,'X_OVTYP/I')
ntuple.Branch('X_OVPROD',X_OVPROD,'X_OVPROD/I')
"""
ntuple.Branch('X_TRACKN',X_TRACKN,'X_TRACKN/I')
ntuple.Branch('X_TRACKL',X_TRACKL,'X_TRACKL/D')
ntuple.Branch('X_TRACKX',X_TRACKX,'X_TRACKX[X_TRACKN]/D')
ntuple.Branch('X_TRACKY',X_TRACKY,'X_TRACKY[X_TRACKN]/D')
ntuple.Branch('X_TRACKZ',X_TRACKZ,'X_TRACKZ[X_TRACKN]/D')
ntuple.Branch('X_TRACKT',X_TRACKT,'X_TRACKT[X_TRACKN]/D')
ntuple.Branch('X_TRACKPX',X_TRACKPX,'X_TRACKPX[X_TRACKN]/D')
ntuple.Branch('X_TRACKPY',X_TRACKPY,'X_TRACKPY[X_TRACKN]/D')
ntuple.Branch('X_TRACKPZ',X_TRACKPZ,'X_TRACKPZ[X_TRACKN]/D')
"""
ntuple.Branch('X_PVX',X_PVX,'X_PVX/D')
ntuple.Branch('X_PVY',X_PVY,'X_PVY/D')
ntuple.Branch('X_PVZ',X_PVZ,'X_PVZ/D')
ntuple.Branch('X_PVT',X_PVT,'X_PVT/D')
ntuple.Branch('X_PVK',X_PVK,'X_PVK/I')

ntuple.Branch('X_ENDVX',X_ENDVX,'X_ENDVX/D')
ntuple.Branch('X_ENDVY',X_ENDVY,'X_ENDVY/D')
ntuple.Branch('X_ENDVZ',X_ENDVZ,'X_ENDVZ/D')
ntuple.Branch('X_ENDVT',X_ENDVT,'X_ENDVT/D')
ntuple.Branch('X_ENDVK',X_ENDVK,'X_ENDVK/I')
ntuple.Branch('X_ENDVTYP',X_ENDVTYP,'X_ENDVTYP/I')
ntuple.Branch('X_ENDVPROD',X_ENDVPROD,'X_ENDVPROD/I')

ntuple.Branch('X_CALOVX',X_CALOVX,'X_CALOVX/D')
ntuple.Branch('X_CALOVY',X_CALOVY,'X_CALOVY/D')
ntuple.Branch('X_CALOVZ',X_CALOVZ,'X_CALOVZ/D')
ntuple.Branch('X_CALOVT',X_CALOVT,'X_CALOVT/D')

ntuple.Branch('X_CALOPX',X_CALOPX,'X_CALOPX/D')
ntuple.Branch('X_CALOPY',X_CALOPY,'X_CALOPY/D')
ntuple.Branch('X_CALOPZ',X_CALOPZ,'X_CALOPZ/D')

def lastVertex(mcp):
    myEndV = 0
    vList = []
    endVs = mcp.endVertices()
    if endVs.data() != None:
      for endV in endVs:
        if endV.target() != None:
            vList.append( ( endV.target() , endV.target().time() ) )
      vList.sort(key=lambda tup: tup[1])
      myEndV = vList[-1][0]
    return myEndV

def getBremVertices(mcp):
  vList = []
  endVs = mcp.endVertices()
  if endVs.data() != None:
    for endV in endVs:
      if endV.target() != None:
        if endV.target().type() == 101 and endV.target().position().z() <= 12520: 
          vList.append( ( endV.target() , endV.target().position().z() ) )
    vList.sort(key=lambda tup: tup[1])
  return vList

def isStable(mcp):
  stable = False
  if mcp.particleID().abspid() == 211 or mcp.particleID().abspid() == 321 or mcp.particleID().abspid() == 2212 or mcp.particleID().abspid() == 11 or mcp.particleID().abspid() == 13 or mcp.particleID().abspid() == 22 or mcp.particleID().abspid == 2112:
    stable = True  
  return stable

def idForExtrapolator(mcp):
    mcPID = 0
    if mcp.particleID().abspid() == 211:
      mcPID = gbl.LHCb.Tr.PID.Pion()
    elif mcp.particleID().abspid() == 321:
      mcPID = gbl.LHCb.Tr.PID.Kaon()
    elif mcp.particleID().abspid() == 2212:
      mcPID = gbl.LHCb.Tr.PID.Proton()
    elif mcp.particleID().abspid() == 11:
      mcPID = gbl.LHCb.Tr.PID.Electron()
    elif mcp.particleID().abspid() == 13:
      mcPID = gbl.LHCb.Tr.PID.Muon()
    else:
      mcPID = gbl.LHCb.Tr.PID.Pion()
    return mcPID

def getPVtime(vList):
  pvList = []
  for v in vList:
    if v.type() == 1:
      pvList += [(v.key(),myRandom.Gaus(0.,90./sqrt(2)/clight))]
  return pvList

def timePV(pvK,pvList):
  myt = 0
  for k,t in pvList:
    if k == pvK:
      myt = t
      break
  return myt

def filterParticle(mcp):
    if mcp.momentum().Z() < 0: return False
    if mcp.particleID().pid() == -99000000: return False
    if mcp.particleID().pid() < -1000000000: return False
    if mcp.particleID().pid() > 1000000000: return False
    if mcp.momentum().P() < 0.01: return False
    if mcp.momentum().Z() < 0.01: return False
    if mcp.originVertex().position().Z() > 12520: return False
    return True

def createState(state,mcp):
  state.setX(mcp.originVertex().position().X())
  state.setY(mcp.originVertex().position().Y())
  state.setZ(mcp.originVertex().position().Z())
  state.setTx(mcp.momentum().X()/mcp.momentum().Z())
  state.setTy(mcp.momentum().Y()/mcp.momentum().Z())
  if mcp.particleID().threeCharge() == 0:
    state.setQOverP(1./mcp.momentum().P())
  else:
    state.setQOverP(mcp.particleID().threeCharge()/3/mcp.momentum().P())

def getBeta(mcp,state):
  return state.p()/sqrt(state.p()**2+mcp.momentum().M2())

def removeBremE(state,newV):
  photon = 0
  for p in newV.products():
    if p.target().particleID().abspid() == 22: photon = p.target()
    break
  if photon == 0:
    return
  newPx = state.momentum().X()-photon.momentum().X()
  newPy = state.momentum().Y()-photon.momentum().Y()
  newPz = state.momentum().Z()-photon.momentum().Z()
  tot = sqrt(newPx*newPx+newPy*newPy+newPz*newPz)
  q = state.qOverP()*state.p()
  Tx = newPx/newPz
  Ty = newPy/newPz
  #print state.momentum().X(),newPx,state.momentum().Y(),newPy,state.momentum().Z(),newPz
  state.setTx(Tx)
  state.setTy(Ty)
  state.setQOverP(q/tot)
  #print state.momentum().X(),newPx,state.momentum().Y(),newPy,state.momentum().Z(),newPz


def extrapolateParticle(mcp,state,endV,caloV,tme,trackPoints,bremList):
  createState(state,mcp)
  startX = state.position().X()
  startY = state.position().Y()
  startZ = state.position().Z()
  startT = mcp.originVertex().time()
  endX = endV.position().X()
  endY = endV.position().Y()
  endZ = endV.position().Z()
  if endV.type() in [0, 2, 3, 4, 100, 102, 105, 106]:
    if endZ > 12520: 
      endZ = 12520.
    else:
      caloV.setPosition(XYZPoint(0,0,0))
      caloV.setTime(-9999)
      return True
  else:
    endZ = 12520.
  endT = endV.time()
  step = (endZ-startZ)/100
  steps = [ startZ+n*step for n in range(1,101) ]
  if steps[-1] > 12520.: steps[-1] = 12520.
  else: steps.append(12520.)
  for s in steps:
    tmpV = MCVertex()
    tmpV.setPosition(XYZPoint(0,0,s))
    tmpV.setType(0)
    bremList.append((tmpV,s))
  bremList.sort(key=lambda tup: tup[1])
  mcCharge = mcp.particleID().threeCharge()
  sc = True
  if mcCharge != 0:
    #while True:
    for newV,newZ in bremList:
      x = state.position().X()
      y = state.position().Y()
      z = state.position().Z()
      #newZ = z+step
      beta = getBeta(mcp,state)
      sc = tme.propagate(state,newZ,idForExtrapolator(mcp))
      if not sc: break #return sc
      distance = (state.position().X()-x)**2+(state.position().Y()-y)**2+(state.position().Z()-z)**2
      startT = startT + sqrt(distance)/clight/beta
      caloV.setPosition(state.position())
      caloV.setTime(startT)
      trackPoints.append( (state.position().X(),state.position().Y(),state.position().Z(),startT,state.momentum().X(),state.momentum().Y(),state.momentum().Z()) )
      if(newV.type() == 101):
        removeBremE(state,newV)
    if sc:
      return sc
    else:
      caloV.setPosition(XYZPoint(0,0,0))
      caloV.setTime(-9999);
      trackPoints.append((0,0,0,0,0,0,0))
      return True
  else:
    #while True:
    for newV,newZ in bremList:
      x = state.position().X()
      y = state.position().Y()
      z = state.position().Z()
      #newZ = z+step
      beta = mcp.momentum().Beta()
      state.linearTransportTo(newZ)
      endX = state.position().X()
      endY = state.position().Y()
      distance = (endX-x)**2+(endY-y)**2+(newZ-z)**2
      startT = startT + sqrt(distance)/beta/clight
      trackPoints.append( (endX,endY,newZ,startT,state.momentum().X(),state.momentum().Y(),state.momentum().Z()) )
    caloV.setPosition(state.position())
    caloV.setTime(startT)
    #print "    NEUTRAL: ",startX,startY,startZ
    #print "             ",state.position().X(),state.position().Y(),state.position().Z()
    #print "             ",state.momentum().X(),state.momentum().Y(),state.momentum().Z()
    #print "             ",caloV.position().X(),caloV.position().Y(),caloV.position().Z()
    #print "             ",distance,beta,startT,endV.time(),caloV.time()
    #print
    return True

def fillTuple(ntuple,mcp,state,endV,caloV,pvList,trackPoints):
  tPV = timePV(mcp.primaryVertex().key(),pvList)
  X_ID[0] = mcp.particleID().pid()
  X_KEY[0] = mcp.key()
  X_MID[0] = 0
  X_MKEY[0] = 0
  X_GMID[0] = 0
  X_GMKEY[0] = 0
  X_GGMID[0] = 0
  X_GGMKEY[0] = 0
  if mcp.mother() != None:
    X_MID[0] = mcp.mother().particleID().pid()
    X_MKEY[0] = mcp.mother().key()
    if mcp.mother().mother() != None:
      X_GMID[0] = mcp.mother().mother().particleID().pid()
      X_GMKEY[0] = mcp.mother().mother().key()
      if mcp.mother().mother().mother() != None:
        X_GGMID[0] = mcp.mother().mother().mother().particleID().pid()
        X_GGMKEY[0] = mcp.mother().mother().mother().key()
  X_PVX[0]    = mcp.primaryVertex().position().X()
  X_PVY[0]    = mcp.primaryVertex().position().Y()
  X_PVZ[0]    = mcp.primaryVertex().position().Z()
  X_PVT[0]    = tPV
  X_PVK[0]    = mcp.primaryVertex().key()

  X_PX[0]     = mcp.momentum().X()
  X_PY[0]     = mcp.momentum().Y()
  X_PZ[0]     = mcp.momentum().Z()
  X_M[0]      = mcp.momentum().M()
  X_OVX[0]    = mcp.originVertex().position().X()
  X_OVY[0]    = mcp.originVertex().position().Y()
  X_OVZ[0]    = mcp.originVertex().position().Z()
  X_OVT[0]    = mcp.originVertex().time()+tPV
  X_OVK[0]    = mcp.originVertex().key()
  X_OVTYP[0]    = mcp.originVertex().type()
  X_OVPROD[0]    = mcp.originVertex().products().size()
  X_ENDVX[0]  = endV.position().X()
  X_ENDVY[0]  = endV.position().Y()
  X_ENDVZ[0]  = endV.position().Z()
  X_ENDVT[0]  = endV.time()+tPV
  X_ENDVTYP[0]  = endV.type()
  X_ENDVPROD[0]  = endV.products().size()
  X_ENDVK[0]  = endV.key()
  X_CALOVX[0] = caloV.position().X()
  X_CALOVY[0] = caloV.position().Y()
  X_CALOVZ[0] = caloV.position().Z()
  X_CALOVT[0] = caloV.time()+tPV
  X_CALOPX[0] = state.momentum().X()
  X_CALOPY[0] = state.momentum().Y()
  X_CALOPZ[0] = state.momentum().Z()
  nPoints = 0
  X_TRACKN[0] = min(NMAX,len(trackPoints))
  """
  length = 0
  for i in range(X_TRACKN[0]):
    X_TRACKX[i] = trackPoints[i][0]
    X_TRACKY[i] = trackPoints[i][1]
    X_TRACKZ[i] = trackPoints[i][2]
    X_TRACKT[i] = trackPoints[i][3]
    X_TRACKPX[i] = trackPoints[i][4]
    X_TRACKPY[i] = trackPoints[i][5]
    X_TRACKPZ[i] = trackPoints[i][6]
    if i == 0:
      length = length + sqrt((X_TRACKX[i]-X_OVX[0])**2+(X_TRACKY[i]-X_OVY[0])**2+(X_TRACKZ[i]-X_OVZ[0])**2)
    else:
      length = length + sqrt((X_TRACKX[i]-X_TRACKX[i-1])**2+(X_TRACKY[i]-X_TRACKY[i-1])**2+(X_TRACKZ[i]-X_TRACKZ[i-1])**2)
  X_TRACKL[0] = length
  """
  ntuple.Fill()


while 1:
  appMgr.run(1)
  head = evtsvc['/Event/MC/Header']
  if head == None: break
  #print head
  runNumber[0] = head.runNumber()
  eventNumber[0] = head.evtNumber()
  tme = toolsvc.create('TrackMasterExtrapolator',interface='ITrackExtrapolator')
  mcparts = evtsvc['/Event/MC/Particles']
  #print mcparts.size()
  mcvs = evtsvc['/Event/MC/Vertices']
  pvList = getPVtime(mcvs)
  for mcp in mcparts:
    trackPoints = []
    if not filterParticle(mcp): continue
    endV = lastVertex(mcp)
    bremList = getBremVertices(mcp)
    if endV != 0:
      #if endV.type() != 2 and endV.type() != 3 and endV.type() != 100 and endV.type() != 4:
      #if endV.position().Z() < 12520: continue
      state = State()
      caloV = MCVertex()
      sc = extrapolateParticle(mcp,state,endV,caloV,tme,trackPoints,bremList)
      if sc: fillTuple(ntuple,mcp,state,endV,caloV,pvList,trackPoints)
      #else:
      #  state = State()
      #  caloV = MCVertex()
      #  sc = extrapolateParticle(mcp,state,endV,caloV,tme)
      #  if sc: fillTuple(ntuple,mcp,state,endV,caloV,pvList)
    else:
      endV = MCVertex()
      endV.setTime(0)
      endV.setType(-1)
      endV.setPosition(XYZPoint(0,0,30000))
      state = State()
      caloV = MCVertex()
      sc = extrapolateParticle(mcp,state,endV,caloV,tme,trackPoints,bremList)
      if sc: fillTuple(ntuple,mcp,state,endV,caloV,pvList,trackPoints)
#  break

tupleFile.WriteTObject(ntuple,"","Overwrite")
tupleFile.Close()


