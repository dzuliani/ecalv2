from Configurables import (
    DaVinci,
    EventSelector,
    PrintMCTree,
    MCDecayTreeTuple
)
from DecayTreeTuple.Configuration import *
#from DecayTreeTuple import Configuration

"""Configure the variables below with:
decay: Decay you want to inspect, using 'newer' LoKi decay descriptor syntax,
decay_heads: Particles you'd like to see the decay tree of,
datafile: Where the file created by the Gauss generation phase is, and
year: What year the MC is simulating.
"""

# https://twiki.cern.ch/twiki/bin/view/LHCb/FAQ/LoKiNewDecayFinders
#decay = "[[B_s0]cc => ^(D_s- => ^K+ ^K- ^pi-) ^pi+]CC"
#decay = "[[B+]cc => ^(J/psi(1S) => ^mu+ ^mu-) ^K+]CC"
#decay = '[Xb -> X ...]CC'
decay = '[X]CC'
#decay = '[Xb -> Xb ...]CC'
#decay_heads = ["B0","B~0","B+","B-","B_s0","B_s~0"]
datafile = "./GaussRaw.xgen"
year = 2016

# For a quick and dirty check, you don't need to edit anything below here.
##########################################################################

# Create an MC DTT containing any candidates matching the decay descriptor
mctuple = MCDecayTreeTuple("MCDecayTreeTuple")
mctuple.Decay = decay
mctuple.addBranches({
        'X': '[X]CC'
    })
mctuple.ToolList = [
    "MCTupleToolHierarchy",
    "LoKi::Hybrid::MCTupleTool/LoKi_Photos",
    #"TupleToolMCTruth",
    "MCTupleToolPID",
    #"MCTupleToolPrimaries",
    #"MCTupleToolPrompt",
    #"MCTupleToolInteractions"
]
# Add a 'number of photons' branch
mctuple.addTupleTool("MCTupleToolKinematic").Verbose = True
mctuple.addTupleTool("LoKi::Hybrid::TupleTool/LoKi_Photos").Variables = {
    "nPhotos": "MCNINTREE(('gamma' == MCABSID))"
}

# Print the decay tree for any particle in decay_heads
#printMC = PrintMCTree()
#printMC.ParticleNames = decay_heads

# Name of the .xgen file produced by Gauss
EventSelector().Input = ["DATAFILE='{0}' TYP='POOL_ROOTTREE' Opt='READ'".format(datafile)]

# Configure DaVinci
DaVinci().TupleFile = "./DaVinciRaw.root"
DaVinci().Simulation = True
DaVinci().Lumi = False
DaVinci().DataType = str(year)
#DaVinci().UserAlgorithms = [printMC, mctuple]
DaVinci().UserAlgorithms = [mctuple]
