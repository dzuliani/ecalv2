#
# Example options to run Gauss in a pure reading mode with generator
# and MCtruth.
# Unpacking of all containers that one wants to access have to be done
# here only MCParticles and MCVertices are done, hence it works for .gen
# or .xgen files
# 
from Gauss.Configuration import *
from Configurables import UnpackMCParticle, UnpackMCVertex
from Configurables import MuonMultipleScatteringChecker

#-- Phases to be executed: initialization + monitor
#
def readOnly():

    #ApplicationMgr().TopAlg = ["LbAppInit/ReadFile",
    #                           "GaudiSequencer/Unpack",
    #                           "GaudiSequencer/GenMonitor",
    #                           ]   

    unpack = GaudiSequencer( "Unpack" )
    unpack.Members = [ 'UnpackMCParticle/UnpackMCParticles','UnpackMCVertex/UnpackMCVertices'
                       ]
    #-- Must read events
    #   Overwrite property of no events input set for "normal" Gauss jobs
    #
    #   ...is set to "NONE" - now where in v37r5?...
    #
    ApplicationMgr().EvtSel = ""

    moni = GaudiSequencer("SimMonitor")

    moni.Members = moni.Members[0:-1]

##     if 'GaudiSequencer/DataUnpackTest' in moni.Members:
##         moni.Members.remove('GaudiSequencer/DataUnpackTest')

    alg = GaudiSequencer("DataUnpackTest")
    if alg in moni.Members:
        moni.Members.remove(alg)
    
    print "before", moni.Members
 
#-- Do the readOnly as a postconfig action for Gauss
#
appendPostConfigAction ( readOnly )

#--General Gauss setting and sequence
#
Gauss().Phases = []
Gauss().OutputType = 'NONE'

#-- Unpack all data
DataOnDemandSvc().AlgMap =  {'/Event/MC/Particles': 'UnpackMCParticle/UnpackMCParticle',
                             'pSim/MCParticles': 'UnpackMCParticle/UnpackMCParticle'
}

#------------------------------------------------------------------------
# Detailed Monitor
#------------------------------------------------------------------------
#Algorithm filling detailed info in an ntuple from HepMC
GaudiSequencer("Monitor").Members += [ "GaudiSequencer/GenMonitor" ]
GaudiSequencer("Monitor").Members += [ "GaudiSequencer/SimMonitor" ]

EventSelector().Input   += [
"DATAFILE='/home/LHCB/dmanuzzi/ECAL/storage/Delphes_Bd_3pi_nu71_20190911/evtType_11102404/0/Gauss-11102404-15ev-20190911.gen' TYP='POOL_ROOTTREE' OPT='READ'",
]

#-- Do not need them here, can use #importOptions($APPCONFIGOPTS/Conditions...)
#LHCbApp( DataType = "2011", Simulation = True )
#LHCbApp().DDDBtag = "head-20100504"
#LHCbApp().CondDBtag = "sim-20100510-vc-md100"

importOptions("/home/LHCB-T3/dmanuzzi/ECAL/Gauss/Sim/LbDelphes/options/Beam7000GeV-md100-nu71.5.py")
#tags
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-mu100"



LHCbApp().EvtMax = 5

