#ifndef myDaVinci_cxx
#define myDaVinci_cxx

#include <myDaVinci.h>
using namespace std;
ClassImp(CaloEvent);

myDaVinci::myDaVinci(){
	m_tin = 0;
	m_tout = 0;
	m_nfout = "";
	m_ncontainers.clear();
	m_Ncontainers = 0;
	m_NullVertex = new Vertex();
	m_NullPart = new myParticle();
	m_NullPart->setOrigVertex(m_NullVertex);
	m_NullPart->setCaloVertex(m_NullVertex);
	m_NullPart->setEndVertex(m_NullVertex);
	m_cluster = 0;

	m_Dvar.clear();
	m_Dfunctors.clear();
	m_nDvar = { "_M"      ,
     			"_PX"     , "_PY"     , "_PZ"     , "_E"     , "_PT"    ,
			    "_CALOPX" , "_CALOPY" , "_CALOPZ" , "_CALOE" , "_CALOPT",
				"_OVX"    , "_OVY"    , "_OVZ"    , "_OVT"   ,
				"_CALOVX" , "_CALOVY" , "_CALOVZ" , "_CALOVT",
				"_ENDVX"  , "_ENDVY"  , "_ENDVZ"  , "_ENDVT" ,
			    "_TRUE_M" ,
			    "_TRUE_PX"    , "_TRUE_PY"    , "_TRUE_PZ"    , "_TRUE_E"     , "_TRUE_PT"    ,
				"_TRUE_CALOPX", "_TRUE_CALOPY", "_TRUE_CALOPZ", "_TRUE_CALOE" , "_TRUE_CALOPT",
				"_TRUE_OVX"   , "_TRUE_OVY"   , "_TRUE_OVZ"   , "_TRUE_OVT"   ,
				"_TRUE_CALOVX", "_TRUE_CALOVY", "_TRUE_CALOVZ", "_TRUE_CALOVT",
				"_TRUE_ENDVX" , "_TRUE_ENDVY" , "_TRUE_ENDVZ" , "_TRUE_ENDVT" };

	m_NDvar = m_nDvar.size();
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[0+k]  = m_part->getM()     ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[1+k] 	= m_part->getOrigPx();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[2+k] 	= m_part->getOrigPy();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[3+k] 	= m_part->getOrigPz();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[4+k] 	= m_part->getOrigE() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[5+k] 	= m_part->getOrigPt();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[6+k] 	= m_part->getCaloPx();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[7+k] 	= m_part->getCaloPy();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[8+k] 	= m_part->getCaloPz();});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[9+k] 	= m_part->getCaloE() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[10+k] = m_part->getCaloPt();});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[11+k] = m_origV->getX() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[12+k] = m_origV->getY() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[13+k] = m_origV->getZ() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[14+k] = m_origV->getT() ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[15+k] = m_caloV->getX() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[16+k] = m_caloV->getY() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[17+k] = m_caloV->getZ() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[18+k] = m_caloV->getT() ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[19+k] = m_endV->getX()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[20+k] = m_endV->getY()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[21+k] = m_endV->getZ()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[22+k] = m_endV->getT()  ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[23+k] = m_matchedPart->getM()      ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[24+k] = m_matchedPart->getOrigPx() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[25+k] = m_matchedPart->getOrigPy() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[26+k] = m_matchedPart->getOrigPz() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[27+k] = m_matchedPart->getOrigE()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[28+k] = m_matchedPart->getOrigPt() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[29+k] = m_matchedPart->getCaloPx() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[30+k] = m_matchedPart->getCaloPy() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[31+k] = m_matchedPart->getCaloPz() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[32+k] = m_matchedPart->getCaloE()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[33+k] = m_matchedPart->getCaloPt() ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[34+k] = m_matchedOrigV->getX() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[35+k] = m_matchedOrigV->getY() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[36+k] = m_matchedOrigV->getZ() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[37+k] = m_matchedOrigV->getT() ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[38+k] = m_matchedCaloV->getX() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[39+k] = m_matchedCaloV->getY() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[40+k] = m_matchedCaloV->getZ() ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[41+k] = m_matchedCaloV->getT() ;});
	
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[42+k] = m_matchedEndV->getX()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[43+k] = m_matchedEndV->getY()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[44+k] = m_matchedEndV->getZ()  ;});
	m_Dfunctors.push_back([this](Int_t k){ m_Dvar[45+k] = m_matchedEndV->getT()  ;});
	
	
	m_Ivar.clear();
	m_Ifunctors.clear();
	m_nIvar = {"_isMatched","_isSig", "_ID", "_MC_MOTHER_ID", "_MC_GD_MOTHER_ID",
			   "_TRUE_isSig", "_TRUE_ID", "_TRUE_MC_MOTHER_ID", "_TRUE_MC_GD_MOTHER_ID", "_NHITS", 
			    "_pi0Type", "_TRUE_pi0Type" };
	m_NIvar = m_nIvar.size();
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[0+k] = (m_matchedPart!=m_NullPart)?1:0;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[1+k] = m_part->getIsSig()         ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[2+k] = m_part->getID()            ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[3+k] = m_part->getMid()           ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[4+k] = m_part->getGmid()          ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[5+k] = m_matchedPart->getIsSig()  ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[6+k] = m_matchedPart->getID()     ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[7+k] = m_matchedPart->getMid()    ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[8+k] = m_matchedPart->getGmid()   ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[9+k] = m_HittingPhotons.size()    ;});	
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[10+k]= m_part->getPi0Type()       ;});
	m_Ifunctors.push_back([this](Int_t k){ m_Ivar[11+k]= m_matchedPart->getPi0Type();});
	
	m_Lvar.clear();
	m_Lfunctors.clear();
	m_nLvar = {     "_KEY",      "_MC_MOTHER_KEY",      "_MC_GD_MOTHER_KEY",
			   "_TRUE_KEY", "_TRUE_MC_MOTHER_KEY", "_TRUE_MC_GD_MOTHER_KEY"};
	m_NLvar = m_nLvar.size();
	m_Lfunctors.push_back([this](Int_t k){ m_Lvar[0+k] = m_part->getKey()  ;});
	m_Lfunctors.push_back([this](Int_t k){ m_Lvar[1+k] = m_part->getMkey() ;});
	m_Lfunctors.push_back([this](Int_t k){ m_Lvar[2+k] = m_part->getGmkey();});
	m_Lfunctors.push_back([this](Int_t k){ m_Lvar[3+k] = m_matchedPart->getKey()   ;});
	m_Lfunctors.push_back([this](Int_t k){ m_Lvar[4+k] = m_matchedPart->getMkey()  ;});
	m_Lfunctors.push_back([this](Int_t k){ m_Lvar[5+k] = m_matchedPart->getGmkey() ;});
	/*
	m_HittingPhotons.clear();
	m_VDvar.clear();
	m_VDfunctors.clear();
	m_nVDvar = { "_V_CALOVX","_V_CALOVY","_V_CALOVZ","_V_CALOVT",
	             "_V_PX","_V_PY","_V_PZ","_V_E"};
	m_NVDvar = m_nVDvar.size();
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[0+k][i] = m_HittingPhotons[i]->getCaloVertexX();
			else m_VDvar[0+k][i] = -999999.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[1+k][i] = m_HittingPhotons[i]->getCaloVertexY();
			else m_VDvar[1+k][i] = -999999.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[2+k][i] = m_HittingPhotons[i]->getCaloVertexZ();
			else m_VDvar[2+k][i] = -999999.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[3+k][i] = m_HittingPhotons[i]->getCaloVertexT();
			else m_VDvar[3+k][i] = -999999.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[4+k][i] = m_HittingPhotons[i]->getCaloPx();
			else m_VDvar[4+k][i] = 0.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[5+k][i] = m_HittingPhotons[i]->getCaloPy();
			else m_VDvar[5+k][i] = 0.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[6+k][i] = m_HittingPhotons[i]->getCaloPz();
			else m_VDvar[6+k][i] = 0.;
		}
	});
	m_VDfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VDvar[7+k][i] = m_HittingPhotons[i]->getCaloE();
			else m_VDvar[7+k][i] = 0;
		}
	});
	
	m_VIvar.clear();
	m_VIfunctors.clear();
	m_nVIvar = { "_V_isSig"};
	m_NVIvar = m_nVIvar.size();
	m_VIfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VIvar[0+k][i] = ((myParticle*)m_HittingPhotons[i])->getIsSig();
			else m_VIvar[0+k][i] = -999999.;
		}
	});
	m_VLvar.clear();
	m_VLfunctors.clear();
	m_nVLvar = { "_V_KEY"};
	m_NVLvar = m_nVLvar.size();
	m_VLfunctors.push_back([this](Int_t k){ 
		for (UInt_t i=0; i<10; ++i){
			if (i<m_HittingPhotons.size()) m_VLvar[0+k][i] = m_HittingPhotons[i]->getKey();
			else m_VLvar[0+k][i] = -1;
		}
	});
	*/
	m_checkMatch = [](){ return false; };
	m_exceptions = []() {};
}
myDaVinci::~myDaVinci(){
	delete m_NullVertex;
	delete m_NullPart;
}

TTree * myDaVinci::getOutTree() {
	return m_tout;
}
vector<myParticle*> myDaVinci::getParts(){
	return m_parts;
}
void myDaVinci::setCheckMatch(function<Bool_t()> checkMatch){
	m_checkMatch = checkMatch;
};
void myDaVinci::setExceptions(function<void()> exceptions){
	m_exceptions = exceptions;
}
void myDaVinci::initialize(TTree *tin, vector<string> ncontainers, 
						   function<vector<myParticle>* (CaloEvent*)> getMotherContainer,
	                       function<vector<myParticle*>(myParticle*)> getPartRefs){
	cout << "myDaVinci::initialize starts\n";
	m_tin = tin;
	//tin->Print();

	m_ncontainers = ncontainers;
	m_Ncontainers = ncontainers.size();
	m_nmother     = m_ncontainers[0];
	m_getPartRefs = getPartRefs;
	m_getMotherContainer = getMotherContainer;
	m_tout = new TTree("ntp", "ntp");
	m_tout->SetDirectory(0);
	m_Dvar = vector<Double_t>(m_NDvar*m_Ncontainers, 0.0);
	m_Ivar = vector<Int_t>   (m_NIvar*m_Ncontainers, 0);
	m_Lvar = vector<Long64_t>(m_NLvar*m_Ncontainers, 0);
	// m_VDvar= vector<vector<Double_t>>(m_NVDvar*m_Ncontainers, vector<Double_t>(10,0.));
	// m_VIvar= vector<vector<Int_t>>   (m_NVIvar*m_Ncontainers, vector<Int_t>   (10,0 ));
	// m_VLvar= vector<vector<Long64_t>>(m_NVLvar*m_Ncontainers, vector<Long64_t>(10,0 ));
	Int_t KD=0, KI=0, KL=0;
	//Int_t KVD=0, KVI=0, KVL=0;
	for (auto ncontainer : m_ncontainers){
    	for (auto nDvar : m_nDvar){
    		auto bname = ncontainer+nDvar;
    		m_tout->Branch( bname.data(), &(m_Dvar[KD]), (bname+"/D").data() );
    		++KD;
    	}
    	for (auto nIvar : m_nIvar){
    		auto bname = ncontainer+nIvar;
    		m_tout->Branch( bname.data(), &(m_Ivar[KI]), (bname+"/I").data() );
    		++KI;
    	}
    	for (auto nLvar : m_nLvar){
    		auto bname = ncontainer+nLvar;
    		m_tout->Branch( bname.data(), &(m_Lvar[KL]), (bname+"/L").data() );
    		++KL;
    	}	
/*
    	if (ncontainer.find("gamma") != string::npos){
    		for (auto nVDvar : m_nVDvar){
    			auto bname = ncontainer+nVDvar;
    			m_tout->Branch( bname.data(), m_VDvar[KVD].data(), (bname+"[10]/D").data() );
    			++KVD;
    		}
    		for (auto nVIvar : m_nVIvar){
    			auto bname = ncontainer+nVIvar;
    			m_tout->Branch( bname.data(), m_VIvar[KVI].data(), (bname+"[10]/I").data() );
    			++KVI;
    		}
    		for (auto nVLvar : m_nVLvar){
    			auto bname = ncontainer+nVLvar;
    			m_tout->Branch( bname.data(), m_VLvar[KVL].data(), (bname+"[10]/L").data() );
    			++KVL;
    		}
    	}
*/
    }
    m_tout->Branch( "evtNumber", &m_evtNumb, "evtNumber/I");
    m_tout->Branch( "runNumber", &m_runNumb, "runNumber/I");
    m_tout->Branch( "isMatched", &m_isMatched, "isMatched/I");
    m_tout->Branch( "isSig", &m_isSig, "isSig/I");
   // m_tout->Print();
    cout << "myDaVinci::initialize ends\n";
}

void myDaVinci::execute(){
	cout << "myDaVinci::execute starts\n";
	CaloEvent *event = new CaloEvent();
	m_tin->SetBranchAddress("evt", &event);
	cout.flush();
    Int_t nevents = m_tin->GetEntries();
 	cout << "Nevents: " << nevents << endl;
 	for (Int_t i=0; i<nevents; ++i){
 	  	event->Clear();
 	  	//cout << "here1\n";
 	  	m_tin->GetEntry(i);
      	//cout << "here2\n";
        if (i%1 ==0){
            cout << "\revent " << i;
            cout.flush();
        }
        //Int_t count=0;
     
    	vector<myParticle> * mothers = m_getMotherContainer(event);        	
    	cout << "Number of mothers: " << mothers->size() << endl;
    	for (auto it_mother = mothers->begin(); it_mother != mothers->end(); ++it_mother){
    		auto mother = &(*it_mother);
    		Int_t KD=0, KI=0, KL=0;
    		//Int_t KVD=0, KVI=0, KVL=0;
    		m_parts = m_getPartRefs(mother); 
    		if (m_parts.size() == (size_t)0) continue;
    		for (auto part : m_parts){
    			if (part == 0){
    				m_part = m_NullPart;
    				m_matchedPart = m_NullPart;
    			} else {
    				m_part = part;
    				m_matchedPart = m_part->getMatchedPart();
    				if (m_matchedPart == 0)
    					m_matchedPart = m_NullPart; 
    			}
    			////////////////////
    			m_exceptions();
			  	////////////////////
    			m_origV = m_part->getOrigVertex();
    			m_caloV = m_part->getCaloVertex();
    			m_endV  = m_part->getEndVertex();
    			m_matchedOrigV = m_matchedPart->getOrigVertex();
    			m_matchedCaloV = m_matchedPart->getCaloVertex();
    			m_matchedEndV  = m_matchedPart->getEndVertex();

    			if (m_origV == 0) m_origV = m_NullVertex;
    			if (m_caloV == 0) m_caloV = m_NullVertex;
    			if (m_endV  == 0) m_endV  = m_NullVertex;
    			if (m_matchedOrigV == 0) m_matchedOrigV = m_NullVertex;
    			if (m_matchedCaloV == 0) m_matchedCaloV = m_NullVertex;
    			if (m_matchedEndV  == 0) m_matchedEndV  = m_NullVertex;
    		//cout << "   here " << m_part->getID() << endl;
    		//cout.flush();		
/*
		    	vector<Cluster*> clusters = ((myParticle*)m_part)->getClusters();
		    	if (clusters.size() == 0) clusters = m_matchedPart->getClusters();
		    	if (part->getID()==22 && clusters.size()>0){
	    			if (clusters.size() != 1) printf("abbiamo un problema!!! ho un fotone con %d cluster associati! \n", (Int_t)clusters.size());
	    			m_cluster = clusters[0];
	    			if (m_cluster == 0)
	    				cout << "l problema e' un altro!! \n";
	    			m_HittingPhotons = m_cluster->getHittingPhotons(0);
	    			//cout << "myDaVinci::m_HittingPhotons = " << m_HittingPhotons.size() << endl;
	    			if (m_HittingPhotons.size() == 0){
	    				cout << "no fotoni\n";
	    				m_HittingPhotons.clear();	
	    			} 
	    			for_each(m_VDfunctors.begin(), m_VDfunctors.end(), [KVD](auto functor){ functor(KVD);});
			    	KVD += m_NVDvar;
			    	for_each(m_VIfunctors.begin(), m_VIfunctors.end(), [KVI](auto functor){ functor(KVI);});
			    	KVI += m_NVIvar;
			    	for_each(m_VLfunctors.begin(), m_VLfunctors.end(), [KVL](auto functor){ functor(KVL);});
			    	KVL += m_NVLvar;
			    } else m_HittingPhotons.clear();
*/			    
			    for_each(m_Dfunctors.begin(), m_Dfunctors.end(), [KD](auto functor){ functor(KD);});
	     		KD += m_NDvar;
	    		for_each(m_Ifunctors.begin(), m_Ifunctors.end(), [KI](auto functor){ functor(KI);});
	    		KI += m_NIvar;
	    		for_each(m_Lfunctors.begin(), m_Lfunctors.end(), [KL](auto functor){ functor(KL);});
	    		KL += m_NLvar;
	    			
    		}



    		m_evtNumb = event->getEvt();
    		m_runNumb = event->getRun();
    		m_isMatched = m_checkMatch();
    		m_isSig = -1;
    		m_matchedPart = m_parts[0]->getMatchedPart();
    		if (m_matchedPart != 0)
    			m_isSig = m_matchedPart->getIsSig();
    		else m_isSig = -1;
    		

    		m_tout->Fill();
    	} 
    }

    //m_tout->Print();
    cout << "Tot. entries :" << m_tout->GetEntries() << endl;
    cout << "myDaVinci::execute ends\n";
	
}


void myDaVinci::finalize(TString nfout){
	cout << "myDaVinci::finalize starts\n";
	TFile fout(nfout, "RECREATE");
	fout.WriteTObject(m_tout, "", "overwrite");
	fout.Print();
	fout.Close();
	delete m_tout;
	cout << "myDaVinci::finalize ends\n";
}





#endif