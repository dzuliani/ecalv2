#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <TChain.h>
#include <TFile.h>
#include <TTree.h>
#include <CaloEvent.h>
#include <Photon.h>
#include <Cell.h>
#include <myParticle.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TProcessID.h>
#include <TLeaf.h>
#include <TF2.h>
#include <sigShape.h>
#include <myDaVinci.h>
using namespace std;

Int_t main(Int_t argc, Char_t *argv[]){
    TString nfin = argv[1];
    TString nfout= argv[2];
    cout << nfin << endl;
    cout << nfout << endl;

	TFile fin(nfin, "READ");
	TTree *tin = (TTree*)fin.Get("ECALevt");
    tin->Print();
	myDaVinci tupler;
    
    function<vector<myParticle>* (CaloEvent*)> getMotherContainer = [](CaloEvent * event){return event->getContainer("pi0_rec"); };

    function<vector<myParticle*>(myParticle*)> getPartRefs = [](myParticle *pi0){
        vector<myParticle*> ret;
        ret.push_back(pi0);
        vector<myParticle*> gammas;
        vector<myProtoParticle*> protoGammas = pi0->get_daughters();
        for_each(protoGammas.begin(), protoGammas.end(), [&gammas](myProtoParticle* protoGamma){gammas.push_back((myParticle*)protoGamma);});
        ret.insert(ret.end(), gammas.begin(), gammas.end());
        return ret;
    };

    tupler.initialize(tin, {"pi0", "gamma1", "gamma2"}, getMotherContainer, getPartRefs);
    tupler.setCheckMatch([&tupler](){
        auto parts = tupler.getParts();
        if (parts[0]->getMatchedPart() == 0) return false;
        else return true;
    });
    tupler.execute();
    tupler.finalize(nfout);
    fin.Close();
	return 1;
}
