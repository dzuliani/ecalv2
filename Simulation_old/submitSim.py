import os
from sys import argv
eventType   =  11102404  ##Bd-> 3pi
#eventType   = 56000034
tag1        = 'STANDALONE_Bd_3pi_nu71_20191126'
tag2        = 'evtType_%s'%(eventType)
tag3        = ''
Nevt_perjob = 50
firstSubjob = 0
lastSubjob  = 1200
runNumber   = 1126
doGen = True
doAna = False
Condor= True
rmOld = False
onTier3= False
#############################################################
config1  = {
    'seed'   :'noSecondSeed', 
    't_res'  : 0.06, 
    'l_cells': (10.1, 20.2, 60.6),
    'Rmol'   : (15,15,15)} 
config2  = {
    'seed'   :'noSecondSeed', 
    't_res'  : 0.06, 
    'l_cells': (20.2, 40.4, 80.8),
    'Rmol'   : (15,15,15)} 
config3  = {
    'seed'   :'noSecondSeed', 
    't_res'  : 0.06, 
    'l_cells': (40.4, 60.6, 121.2),
    'Rmol'   : (35,35,35)} 
config4  = {
    'seed'   :'noSecondSeed', 
    't_res'  : 0.06, 
    'l_cells': (15.15, 80.8, 121.2),
    'Rmol'   : (15,35,35)} 
config10  = {
    'seed'   :'CorrectSecondSeeds', 
    't_res'  : 0.06, 
    'l_cells': (10.1, 20.2, 60.6),
    'Rmol'   : (15,15,15)} 
config20  = {
    'seed'   :'CorrectSecondSeeds', 
    't_res'  : 0.06, 
    'l_cells': (20.2, 40.4, 80.8),
    'Rmol'   : (15,15,15)} 
config30  = {
    'seed'   :'CorrectSecondSeeds', 
    't_res'  : 0.06, 
    'l_cells': (40.4, 60.6, 121.2),
    'Rmol'   : (35,35,35)} 
config40  = {
    'seed'   :'CorrectSecondSeeds', 
    't_res'  : 0.06, 
    'l_cells': (15.15, 80.8, 121.2),
    'Rmol'   : (15,35,35)} 

configs = [config1, config2, config3, config4,config10, config20, config30, config40]
#configs = [config1]
configs = []
#############################################################
firstEvtNumber = firstSubjob*Nevt_perjob + 1



####### expand job list #######
subjobs_ids = range(firstSubjob,lastSubjob)
tag_configs = []
tag_configs_all = ''
for config in configs:
    seed   = config['seed']
    t_res  = config['t_res']
    l_cells= config['l_cells']
    Rmol   = config['Rmol']
    tag_config  = '%s-%d-l-%.2f_%.2f_%.2f-Rm-%d_%d_%d%s'%(seed,int(t_res*1000), l_cells[0],l_cells[1],l_cells[2], int(Rmol[0]),int(Rmol[1]),int(Rmol[2]),tag3)
    tag_configs.append(tag_config)
    tag_configs_all += tag_config+' '
tag_configs_all = tag_configs_all.replace(' ', ', ')[:-2]

####### create folder architecture #######
path_storage = '/storage'
if onTier3: path_storage = ''
path_output = path_storage+'/gpfs_data/local/lhcb/users/dmanuzzi/ECAL/{tag1}/{tag2}/'.format(tag1=tag1,tag2=tag2)
path_inputs = '/home/LHCB-T3/dmanuzzi/newECAL/Simulation/{tag1}/'.format(tag1=tag1)
if not os.path.exists(path_output): 
        os.makedirs(path_output)
        os.makedirs(path_output+'/log')
       	os.makedirs(path_output+'/err')
       	os.makedirs(path_output+'/out')
else:
    if path_storage+'/gpfs_data/local/lhcb/users/dmanuzzi/ECAL/{tag1}/{tag2}'.format(tag1=tag1, tag2=tag2) in path_output:
        if rmOld:
            os.system('rm -r %s'%(path_output))
if not os.path.exists(path_inputs): 
        os.makedirs(path_inputs)
else:  
    if tag2 in path_inputs: 
        if rmOld:
            os.system('rm -r %s/*'%(path_inputs))

for subjob_id in subjobs_ids:
    path_subjob = path_output+'%d/'%(subjob_id)
    if not os.path.exists(path_subjob): os.makedirs(path_subjob)
    for tag_config in tag_configs:
        path_subjob_conf = path_subjob+'%s/'%(tag_config)
        if not os.path.exists(path_subjob_conf): os.makedirs(path_subjob_conf)
 
############################
f_gauss_job_template = open(path_inputs+'Gauss-Job_parallel.txt', 'r')
s_gauss_job_template = f_gauss_job_template.read()
f_gauss_job_template.close()

for i in subjobs_ids:
    s_gauss_job = s_gauss_job_template.format(eventType=eventType,
                                              Nevt=Nevt_perjob,
                                              runNumber=runNumber,
                                              FirstEvtNumber=int(firstEvtNumber+i*Nevt_perjob)
    )
    f_gauss_job = open(path_output+'/%d/Gauss-Job_parallel.py'%(i), 'w')
    f_gauss_job.write(s_gauss_job)
    f_gauss_job.close()
####### write run.sh #######
f_run = open(path_inputs+'run.sh', 'w')
f_run.write('#!/bin/bash \n')
f_run.write('export CMTCONFIG=x86_64-centos7-gcc8-opt\n')
f_run.write('. /cvmfs/lhcb.cern.ch/lib/LbLogin.sh\n')
f_run.write('subjobs_ids=(')
for subjob_id in subjobs_ids: 
	f_run.write(' %d'%subjob_id)
f_run.write(' )\n')
f_run.write('subjob_i=${subjobs_ids[$1]}\n')
f_run.write('tag_config=$2\n')
f_run.write('echo $1\n')
f_run.write('echo $2\n')
f_run.write('path_output_gen=%s/$subjob_i/\n'           %path_output)
f_run.write('path_output_ana=%s/$subjob_i/$tag_config\n'%path_output)
f_run.write('cd $path_output_gen\n')

if (doGen): 
    f_run.write('lb-run Gauss/v53r2 gaudirun.py $path_output_gen/Gauss-Job_parallel.py \n')
    f_run.write('rm ./GaussRaw.xgen\n')
    f_run.write('mv ./*50ev*.xgen ./GaussRaw.xgen\n')
    f_run.write('{path_inputs}/../DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt/run gaudirun.py {path_inputs}/DaVinci-Job_parallel.py\n'.format(path_inputs=path_inputs))
    f_run.write('%s/../addPVtime $((%d+$subjob_i*%d))\n'%(path_inputs, firstEvtNumber,Nevt_perjob))
    f_run.write('rm ./DaVinciRaw.root\n'.format(path_inputs=path_inputs))
#if (doAna):
    #if onTier3:
    #    f_run.write('root -l -b -q %s/myTest.cpp+ \n'%(path_subjob_conf))
    #else: "'(\""
    #f_run.write("lb-run ROOT/6.14.04 root -l -b -q %s/myTest.cpp+\\(\\\"$tag_config\\\"\\) \n"%(path_inputs))
    #f_run.write("lb-run ROOT/6.14.04 %s/myTest $tag_config \n"%(path_inputs))
f_run.close()
os.system('chmod +x %s/run.sh'%(path_inputs))


##### write condor_subimt #####
f_sub = open(path_inputs+'condor_config.sub', 'w')
if onTier3:
    f_sub.write('Requirements = (machine == "wn-206-04-03-01-a.cr.cnaf.infn.it") || (machine == "wn-206-04-03-02-a.cr.cnaf.infn.it") || (machine == "wn-206-04-03-03-a.cr.cnaf.infn.it") || (machine == "wn-206-04-03-04-a.cr.cnaf.infn.it") || (machine == "wn-206-04-05-01-a.cr.cnaf.infn.it") || (machine == "wn-206-04-05-02-a.cr.cnaf.infn.it") || (machine == "wn-206-04-05-03-a.cr.cnaf.infn.it") || (machine == "wn-206-04-05-04-a.cr.cnaf.infn.it") || (machine == "wn-206-04-07-01-a.cr.cnaf.infn.it") || (machine == "wn-206-04-07-02-a.cr.cnaf.infn.it") || (machine == "wn-206-04-07-03-a.cr.cnaf.infn.it") || (machine == "wn-206-04-07-04-a.cr.cnaf.infn.it") || (machine == "wn-206-04-09-01-a.cr.cnaf.infn.it") || (machine == "wn-206-04-09-02-a.cr.cnaf.infn.it") || (machine == "wn-206-04-09-03-a.cr.cnaf.infn.it") \n')
f_sub.write('executable            = %s/run.sh   \n'%(path_inputs))
f_sub.write('getenv                = True\n')
f_sub.write('arguments             = $(Step) $(Item)\n')
f_sub.write('output                = %s/out/$(Step).$(Item).out   \n'%(path_output))
f_sub.write('error                 = %s/err/$(Step).$(Item).err   \n'%(path_output))
f_sub.write('log                   = %s/log/$(Step).$(Item).log   \n'%(path_output))
f_sub.write('request_memory        = 3 GB   \n')
f_sub.write('+JobFlavour           = "longlunch"   \n')
f_sub.write('Priority              = 1   \n')
f_sub.write('Universe              = vanilla   \n')
if tag_configs_all == '':
    f_sub.write('queue  %d\n'%(len(subjobs_ids)))
else:
    f_sub.write('queue  %d in (%s)\n'%(len(subjobs_ids),tag_configs_all))
f_sub.close()


##### submit all jobs #####
if Condor:
    os.system('condor_submit %s/condor_config.sub'%path_inputs)
else:
    os.system(path_inputs+'run.sh 0 %s'%(tag_configs[0]))











