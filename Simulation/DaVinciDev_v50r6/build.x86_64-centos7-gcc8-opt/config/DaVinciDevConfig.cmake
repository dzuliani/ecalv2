# File automatically generated: DO NOT EDIT.
set(DaVinciDev_heptools_version 96b)
set(DaVinciDev_heptools_system x86_64-centos7-gcc8)

set(DaVinciDev_PLATFORM x86_64-centos7-gcc8-opt)

set(DaVinciDev_VERSION v50r6)
set(DaVinciDev_VERSION_MAJOR 50)
set(DaVinciDev_VERSION_MINOR 6)
set(DaVinciDev_VERSION_PATCH )

set(DaVinciDev_USES DaVinci;v50r6)
set(DaVinciDev_DATA )

list(INSERT CMAKE_MODULE_PATH 0 ${DaVinciDev_DIR}/cmake)
include(DaVinciDevPlatformConfig)
