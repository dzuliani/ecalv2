#ifndef ROOT_Vertex_cxx
#define ROOT_Vertex_cxx
#include <Vertex.h>

//definisce funzioni che ritornano coordinate x,y,z e tempo del vertice

Vertex::Vertex(Double_t x,Double_t y,Double_t z,Double_t t){
	m_ID=-99999;
	m_x=x;
	m_y=y;
	m_z=z;
	m_t=t;
}
void Vertex::initialise(){
	m_ID=-99999;
	m_x=0.0;
	m_y=0.0;
	m_z=0.0;
	m_t=-99999.0;

}

array<Double_t,3> Vertex::getPos(){
	array<Double_t,3> v = {m_x, m_y, m_z};
	return v;
}
array<Double_t,4> Vertex::getPosT(){
	array<Double_t,4> v = {m_x, m_y, m_z, m_t};
	return v;
}

void Vertex::setID(UInt_t run, ULong64_t evt, Long64_t v){
	m_ID = Long64_t( Long64_t(((Long64_t)run)<<48) | Long64_t(evt<<32) |  Long64_t(v) );
}

#endif
