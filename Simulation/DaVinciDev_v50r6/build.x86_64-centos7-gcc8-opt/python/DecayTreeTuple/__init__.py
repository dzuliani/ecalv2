
import os, sys
__path__ = [d for d in [os.path.join(d, 'DecayTreeTuple') for d in sys.path if d]
            if (d.startswith('/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt') or
                d.startswith('/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6')) and
               (os.path.exists(d) or 'python.zip' in d)]
fname = '/afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/Phys/DecayTreeTuple/python/DecayTreeTuple/__init__.py'
if os.path.exists(fname):
    with open(fname) as f:
        code = compile(f.read(), fname, 'exec')
        exec(code)
