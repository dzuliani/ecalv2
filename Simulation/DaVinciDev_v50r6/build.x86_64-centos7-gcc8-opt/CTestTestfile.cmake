# CMake generated Testfile for 
# Source directory: /afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6
# Build directory: /afs/cern.ch/user/d/dzuliani/NEWCAL/ecalv2/Simulation/DaVinciDev_v50r6/build.x86_64-centos7-gcc8-opt
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("Phys/DecayTreeTupleBase")
subdirs("Phys/DecayTreeTupleMC")
subdirs("Phys/DecayTreeTuple")
