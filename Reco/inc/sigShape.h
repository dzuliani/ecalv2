#ifndef ROOT_sigShape_h
#define ROOT_sigShape_h

#include <iostream>
#include <fstream>
#include <vector>
#include "TFile.h"
#include "TTree.h"
#include "TString.h"
#include "TGraph.h"
#include "TMath.h"
#include "TRandom3.h"
#include "TH2D.h"
#include "TProfile.h"
#include "TGraphErrors.h"
using namespace std;

struct EntryTimes {
	Int_t m_entry;
	Float_t m_t_pmt;
	Float_t m_t_ref;
	EntryTimes(){}; 
	EntryTimes(Int_t entry, Float_t t_pmt, Float_t t_ref);
};


class sigShape: public TObject {
public:
	TString            m_nfin;
	TString            m_nfin_refSelected;
	Float_t            m_dt_sampling;
	vector<EntryTimes> m_iEvents;
	vector<Float_t>    m_y_mean;
	vector<Float_t>    m_y_mean_err;
	TRandom3           RND;
	static TRandom3    RNDstat;
	//TH2D*              m_h_OrigShapes;
	TProfile*              m_h_OrigShapes;
	TH1D*                  m_h_t_ref;
	TH1D*                  m_h_t_ref_res;
	TH1D*                  m_h_res;
	TH1D*                  m_h_res_2;
	TGraphErrors*          m_g_shape;
	sigShape(TString nfin, TString nfin_refSelected="none");
	~sigShape();
	void readSelected();

	//void doSelection(TString nfout, Float_t amplMin, Float_t amplMax);

	static Float_t  getTime_lev(Float_t *x, Float_t *y, Float_t &lvl, Float_t lev=0.5, Int_t np=1024);
	static Float_t  fitTime_lev(Float_t *x, Float_t *y, Float_t &lvl, Float_t lev=0.5, Int_t np=1024);
	static Float_t  fitTime_lev(Double_t *x, Double_t *y, Float_t &lvl, Double_t lev=0.5, Int_t np=1024);
	
	static void Traslate(TGraph* g_in, Float_t dx, Double_t *y_out, Int_t np=500);
	static void buildSignal(Double_t *sig_mean, Double_t *sig_mean_err, Double_t *sig_out, Double_t E, Int_t np);
	static void buildSignal(Double_t factor,Double_t *sig_mean, Double_t *sig_mean_err, Double_t *sig_out, Double_t E, Int_t np);
	void GetMinMax(Float_t* y_in, Float_t &Min, Float_t &Max, Int_t np=1024);
	Float_t GetPlateau(Float_t *x_in, Float_t *y_in, Float_t xmin, Float_t xmax, Int_t np=1024);
	void shift(Float_t *x_in, Float_t *y_in, Float_t dt, Float_t *y_out, Float_t yMax, Int_t np=1024);
	void getAverageShape(Int_t np=1024);
	void saveAverageShape(TString nfout);
	void buildSignal(Float_t *sig_out, Int_t np=1024);
	void buildSignal(Float_t *sig_out, Float_t E, Int_t np);
	
	void test0(Int_t Nesp, TString nfout, TString opt_fout="RECREATE");

	TGraphErrors test_ResAtE(vector<Float_t> E, /*TString nfout,*/ UInt_t d_sampling=1, Int_t Nesp=1000);
	TH1D  getResBaseline(UInt_t d_sampling=1, Int_t np=1024);

	ClassDef(sigShape,1)
};

#endif
