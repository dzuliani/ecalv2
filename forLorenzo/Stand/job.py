from Gaudi.Configuration import *
importOptions("Gauss-Job.py")
importOptions("GenStandAlone.py")
importOptions("Gauss-Upgrade-Baseline-20150522.py")
importOptions("Beam7000GeV-md100-nu71.5.py")
importOptions("Pythia8.py")
importOptions("$DECFILESOPTS/11102404.py")

from Gauss.Configuration import *
gaussGen = GenInit("GaussGen")
gaussGen.FirstEventNumber = 1
gaussGen.RunNumber = 0
