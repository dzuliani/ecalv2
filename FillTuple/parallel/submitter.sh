#!/bin/bash
Nevt_perJob=20
firstSubjob=0
lastSubjob=299
fullSim=0

a0=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35
a1=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.6
a2=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.4
a3=upgrade2-Correct2Seed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.2

b0=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35
b1=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.6
b2=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.4
b3=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.2
b00=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_1.0-TresFixed
b01=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.6-TresFixed
b02=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.4-TresFixed
b03=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.2-TresFixed

b10=upgrade2-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_0.0-Tres0

c0=upgrade0-noSecondSeed-l_20.2_40.4_80.8-Rm_15_35_35
c01=upgrade0-noSecondSeed-l_20.2_40.4_80.8-Rm_15_35_35-TresFactor_1.0-TresFixed

d0=upgrade0-BaselineSeed-l_20.2_40.4_80.8-Rm_15_35_35


rm jobs.txt

rm ./condor_log/*/*
#touch ./fillTuple*
#make fillTuple
#make fillTuplePi0
#make fillTuplePi0True
#make fillTupleBd3pi
#make fillTupleBd3piTrue

for mode in $b0 $d0; do
    for i in $(seq $firstSubjob $lastSubjob); do
	#echo $i, $Nevt_perJob, $mode, $fullSim
	echo $i, $Nevt_perJob, $mode, $fullSim >> jobs.txt;
    done;
done
condor_submit submit.jdl
