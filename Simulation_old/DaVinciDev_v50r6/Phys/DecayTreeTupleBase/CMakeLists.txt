###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
################################################################################
# Package: DecayTreeTupleBase
################################################################################
gaudi_subdir(DecayTreeTupleBase)

gaudi_depends_on_subdirs(Calo/CaloUtils
                         Kernel/MCInterfaces
                         Phys/DaVinciKernel
                         Phys/LoKiPhys
                         Phys/LoKiPhysMC)

find_package(Boost)

find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS})

gaudi_add_library(DecayTreeTupleBaseLib
                  src/lib/*.cpp
                  PUBLIC_HEADERS DecayTreeTupleBase
                  INCLUDE_DIRS Boost Kernel/MCInterfaces
                  LINK_LIBRARIES Boost CaloUtils DaVinciKernelLib LoKiPhysLib LoKiPhysMCLib)

gaudi_add_module(DecayTreeTupleBase
                 src/component/*.cpp
                 INCLUDE_DIRS Boost Kernel/MCInterfaces
                 LINK_LIBRARIES Boost CaloUtils DaVinciKernelLib LoKiPhysLib LoKiPhysMCLib DecayTreeTupleBaseLib)

