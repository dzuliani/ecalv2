#ifndef ROOT_Cluster_h
#define ROOT_Cluster_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <vector>
#include <array>
#include <algorithm>
#include <TRefArray.h>
#include <TGraphErrors.h>
#include <myProtoParticle.h>
#include <Cell.h>

using namespace std;


class Cluster: public TObject {
private:
	Int_t m_ID;
	Double_t m_x;
	Double_t m_y;
	Double_t m_z;
	Double_t m_e;
	Double_t m_t;
	vector<Cell*> m_cells;
	Int_t m_ncells;
	map<Int_t, Int_t> m_cellMap;
	
	Double_t m_calib_e;
	Double_t m_calib_x;
	Double_t m_calib_y;
	void calibrate(Calibrations * calibs);

	myProtoParticle * m_RecParticle;
public:
	vector<Int_t>    m_vec_cellIDs;
	vector<Double_t> m_vec_e;
	
	Cluster();
	Cluster(Cell *SeedCell);
	void setZ(Double_t z){ m_z = z; }
	void addCell(Cell *cell);
	Int_t getID(){ return m_ID; }
	void setID(Int_t ID){m_ID = ID;}
	virtual ~Cluster(){}
	void finalize(Calibrations* calibs=0);
	Cell* getSeed();
	Int_t getSeedRegion() { return (m_ID & 3); }
	vector<Cell*> getCells() { return m_cells; }
	vector<Int_t> &getCellIDs(){ return m_vec_cellIDs; }
	vector<Double_t> &getCellEs() { return m_vec_e; }
	Cell * getCellFromID(Int_t id);
	Int_t getCellIndexFromID(Int_t id) { return m_cellMap.at(id);}
	Double_t getX() { return m_x;}
	Double_t getY() { return m_y;}
	Double_t getZ() { return m_z;}
	Double_t getT() { return m_t;}
	Double_t getE() { return m_e;}
	void setE_fromIndex(Int_t Index, Double_t value){ m_vec_e[Index]=value; }
	void setE_fromID(Int_t id, Double_t value);
	Double_t getCalibE(){ return m_calib_e; }
	Double_t getCalibX(){ return m_calib_x; }
	Double_t getCalibY(){ return m_calib_y; }
	myProtoParticle* getCloserPhoton(Double_t Eth=1);
	vector<myProtoParticle*> getHittingPhotons(Double_t Eth=1);
	Int_t getNphotons();
	Double_t getPx();
	Double_t getPy();
	Double_t getPz();
	Double_t getPt();
	Double_t getPt2();

	void setRecParticle(myProtoParticle * part) {m_RecParticle = part;}
	myProtoParticle* getRecParticle() { return m_RecParticle; }
	
	ClassDef(Cluster,1)
};

#endif