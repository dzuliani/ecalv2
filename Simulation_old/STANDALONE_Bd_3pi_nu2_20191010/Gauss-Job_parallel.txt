#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#

from Gaudi.Configuration import *
from Configurables import Gauss, ApplicationMgr, UpdateManagerSvc
from Gauss.Configuration import *    

#add general configurations
#beam config
importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py")
#importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-md100-2012-fix1.py")
#importOptions("$APPCONFIGOPTS/Gauss/Beam7000GeV-md100-nu7.6.py")
#importOptions("/home/LHCB-T3/dmanuzzi/ECAL/Gauss/Sim/LbDelphes/options/Beam7000GeV-md100-nu71.5.py")
#tags
from Configurables import LHCbApp
LHCbApp().DDDBtag   = "dddb-20170721-3"
LHCbApp().CondDBtag = "sim-20170721-2-vc-mu100"
###choose event type
eventType = '{eventType}'
#use PGun sim or pythia?
importOptions('$DECFILESOPTS/%s.py'%eventType)
#get Pgun options if necessary, otherwise load pythia8
pGun= (int(eventType)>50000000)

if True==pGun:    
    importOptions("$LBPGUNSROOT/options/PGuns.py")
    from Configurables import ToolSvc
    from Configurables import EvtGenDecay
    from Configurables import ParticleGun

    #if the dec file already has a particle gun configuration, pass it here, else, configure a flat momentum spectrum
    if hasattr(ParticleGun(),'SignalPdgCode'):
        print 'has attribute!'
        #no configuration necessary
        pass
    elif hasattr(ParticleGun(),'MomentumRange'):
        if hasattr(ParticleGun().MomentumRange,"PdgCodes"):
            print 'got PDGCodes. Should be Configured'
            pass
        else:
            print 'problem with configuration!'
            import sys
            sys.exit()
    else:
        print 'using flat momentum spectrum!'
        from Configurables import MomentumRange
        ParticleGun().addTool( MomentumRange )
        from GaudiKernel import SystemOfUnits
        ParticleGun().MomentumRange.MomentumMin = 1.0*SystemOfUnits.GeV
        from GaudiKernel import SystemOfUnits
        ParticleGun().MomentumRange.MomentumMax = 100.*SystemOfUnits.GeV
        ParticleGun().EventType = eventType
                
        ParticleGun().ParticleGunTool = "MomentumRange"
        ParticleGun().NumberOfParticlesTool = "FlatNParticles"
        #figure this out
        from Configurables import Generation
        pid_list = []
        if hasattr(Generation(),'SignalRepeatedHadronization'):
            pid_list = Generation().SignalRepeatedHadronization.SignalPIDList
        elif hasattr(Generation(),"SignalPlain"):
            pid_list = Generation().SignalPlain.SignalPIDList
        else:
            print 'major configuration problem, please fix!'
            import sys
            sys.exit()
        print 'got signal PID list',pid_list
        ParticleGun().MomentumRange.PdgCodes = pid_list
        ParticleGun().SignalPdgCode = abs(pid_list[0])
        ParticleGun().DecayTool="EvtGenDecay"
    
else:
    importOptions('$DECFILESOPTS/%s.py'%eventType)
    importOptions("$LBPYTHIA8ROOT/options/Pythia8.py")
    #fix pythia 8 color reconnection problem
    importOptions("$APPCONFIGOPTS/Gauss/TuningPythia8_Sim09.py")
    
importOptions('$GAUSSOPTS/GenStandAlone.py')
            
#--Number of events
nEvts = {Nevt}
LHCbApp().EvtMax = nEvts

#--Generator phase, set random numbers
gaussGen = GenInit("GaussGen")
gaussGen.FirstEventNumber = {FirstEvtNumber}
gaussGen.RunNumber        = {runNumber}



#--Set name of output files for given job and read in options
idFile = 'Gauss_'+str(eventType)
HistogramPersistencySvc().OutputFile = idFile+'_histos.root'
#--- Save ntuple with hadronic cross section information
ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='GaussTuple_%s.root' TYP='ROOT' OPT='NEW'"%(eventType)]
       
FileCatalog().Catalogs = [ "xmlcatalog_file:Gauss_LbDelphes_0.xml" ]


#write to TES
def writeHits():    
    OutputStream("GaussTape").ItemList.append("/Event/MC/Vertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/Particles#1")
    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCParticles#1")
    OutputStream("GaussTape").ItemList.append("/Event/MCFast/MCVertices#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/ProtoP/Charged#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/ProtoP/Neutrals#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Track/Best#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Rich/PIDs#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Calo/Photons#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Calo/EcalClusters#1")
    OutputStream("GaussTape").ItemList.append("/Event/MC/DigiHeader#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/RawEvent#1")
    OutputStream("GaussTape").ItemList.append("/Event/DAQ/ODIN#1")
    OutputStream("GaussTape").ItemList.append("/Event/Rec/Vertex/Primary#1")
    print(OutputStream("GaussTape"))
#appendPostConfigAction(writeHits)

