#ifndef ROOT_myDaVinci_h
#define ROOT_myDaVinci_h

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <TClonesArray.h>
#include "Photon.h"
#include "Cell.h"
#include "Cluster.h"
#include "CaloEvent.h"
#include <TChain.h>
#include <TF2.h>
#include <TRandom3.h>
#include "myParticle.h"
#include <vector>
#include <string>
using namespace std;



class myDaVinci {
private:
	TTree *m_tin;
	TTree *m_tout;
	TString m_nfout;
	vector<string> m_ncontainers;
	Int_t m_Ncontainers;
	function<vector<myParticle*>(myParticle*)> m_getPartRefs;
	function<vector<myParticle>* (CaloEvent*)> m_getMotherContainer;
	function<void()> m_exceptions;
	//Double	
	vector<string>  m_nDvar;
	vector<function<void(Int_t)>> m_Dfunctors;
	vector<Double_t> m_Dvar;
	Int_t m_NDvar;
	//Int
	vector<string>  m_nIvar;
	vector<function<void(Int_t)>> m_Ifunctors;
	vector<Int_t> m_Ivar;
	Int_t m_NIvar;
	//Long64_t
	vector<string>  m_nLvar;
	vector<function<void(Int_t)>> m_Lfunctors;
	vector<Long64_t> m_Lvar;
	Int_t m_NLvar;

	//vector<Double_t>
	vector<string> m_nVDvar;
	vector<function<void(Int_t)>> m_VDfunctors;
	vector<vector<Double_t>> m_VDvar;
	Int_t m_NVDvar;
	//vector<Int_t>
	vector<string> m_nVIvar;
	vector<function<void(Int_t)>> m_VIfunctors;
	vector<vector<Int_t>> m_VIvar;
	Int_t m_NVIvar;
	//vector<Long64_t>
	vector<string> m_nVLvar;
	vector<function<void(Int_t)>> m_VLfunctors;
	vector<vector<Long64_t>> m_VLvar;
	Int_t m_NVLvar;

	Int_t m_evtNumb, m_runNumb, m_isMatched, m_isSig;

	string m_nmother;
	vector<myParticle*> m_parts;
	function<Bool_t()> m_checkMatch;
	myParticle *m_part;
	myParticle *m_matchedPart;
	Vertex* m_origV;
	Vertex* m_caloV;
	Vertex* m_endV;
	Vertex* m_matchedOrigV;
	Vertex* m_matchedCaloV;
	Vertex* m_matchedEndV;
	myParticle *m_NullPart;
	Vertex* m_NullVertex;
	Cluster* m_cluster;
	vector<myProtoParticle*> m_HittingPhotons;
public:
	myDaVinci();
	~myDaVinci();
	
	TTree * getOutTree();
	vector<myParticle*> getParts();
	void initialize(TTree *tin, vector<string> ncontainers,
				    function<vector<myParticle>* (CaloEvent*)> getMotherContainer,
	                function<vector<myParticle*>(myParticle*)> getPartRefs);
	void execute();
	void finalize(TString nfout);

	void setCheckMatch(function<Bool_t()> checkMatch);
	void setExceptions(function<void()> exceptions);
	vector<Double_t>* getDvars() { return &m_Dvar; }
	vector<Int_t>*    getIvars() { return &m_Ivar; }
	vector<Long64_t>* getLvars() { return &m_Lvar; }
	Int_t getNDvar() { return m_NDvar; }
	Int_t getNIvar() { return m_NIvar; }
	Int_t getNLvar() { return m_NLvar; }

};

#endif