#ifndef ROOT_myParticle_h
#define ROOT_myParticle_h
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

#include <TVector3.h>
#include <TLorentzVector.h>

#include <Cluster.h>
#include <myProtoParticle.h>
using namespace std;

class myParticle : public myProtoParticle {
	private:
		vector<Cluster*> m_clusters;
		myParticle * m_matchedPart;
		Int_t m_isSig;
		Int_t m_pi0type; //1) merged, 2) reolved
	public:
		myParticle();
		virtual ~myParticle() {};
		myParticle(TLorentzVector &mom, Double_t mass);

		void getMomIfOrigV(Double_t &px, Double_t &py, Double_t &pz, Double_t x, Double_t y, Double_t z);
		void addCluster(Cluster* v) { m_clusters.push_back(v); }
    	vector<Cluster*> getClusters() { return m_clusters; }
    	void setMatchedPart(myParticle *part){ m_matchedPart = part; }
    	myParticle* getMatchedPart(){ return m_matchedPart; }
    	
    	Int_t getIsSig() { return m_isSig; }
    	void setIsSig(Int_t v) { m_isSig = v; }

    	Int_t getPi0Type() { return m_pi0type;}
    	void setPi0Type(Int_t v) { m_pi0type = v; } 
    	ClassDef(myParticle,1)
};

#endif
