from Gaudi.Configuration import *
importOptions("Gauss-Job.py")
importOptions("Gauss-Full.py")
importOptions("Beam7000GeV-md100-nu71.5.py")
importOptions("Pythia8.py")
importOptions("$DECFILESOPTS/42122000.py")	#modificato l'event type

from Gauss.Configuration import *
gaussGen = GenInit("GaussGen")
gaussGen.FirstEventNumber = 1
gaussGen.RunNumber = 0
