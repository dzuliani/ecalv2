#!/bin/bash

. $VO_LHCB_SW_DIR/lib/LbEnv

index=$1

lb-run -c x86_64-slc6-gcc49-opt Gauss/v51r2 gaudirun.py job.py


fileName=`ls *.sim`
lb-run DaVinci/v50r6 python readFullBrem.py -i $fileName
